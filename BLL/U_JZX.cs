﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common; 

namespace BLL{ 
	public class U_JZX
    {
		#region 增加
		
		public bool Add(string name,int rongliang,int chuliang,string riqi,int addren,DateTime addtime,int daynum,string xiangbian,DateTime chudate,int pdaid,int isFin,int xid,DbTransaction tran)
		{
            return new DAL.U_JZX().Add(name, rongliang, chuliang, riqi, addren, addtime, daynum, xiangbian, chudate, pdaid, isFin, xid, tran) != 0;
		}
		#endregion
		 
		#region 修改

        public bool Update(int id, string name, int rongliang, int chuliang, string riqi, int addren, DateTime addtime, int daynum, string xiangbian, DateTime chudate, int pdaid, int isFin, int xid, DbTransaction tran)
		{
            return new DAL.U_JZX().Update(id, name, rongliang, chuliang, riqi, addren, addtime, daynum, xiangbian, chudate, pdaid, isFin, xid, tran) == 1;
		}
        #endregion		
		 
		#region 查询
		
		public Model.U_JZX GetModelByCon(string where,DbTransaction tran)
		{
		return new DAL.U_JZX().GetModelByCon(where,tran);
		}
		public List<Model.U_JZX> GetListModel(string where)
		{
		return new DAL.U_JZX().GetListModel(where);
		}
		public Model.U_JZX GetModel(int id)
		{
			return new DAL.U_JZX().GetModel(id);
		}
        public int GetMaxIDAtToday(string riqi,DbTransaction tran)
        {
            return new DAL.U_JZX().GetMaxIDAtToday(riqi, tran);
        }
		#endregion		
		
		#region  删除

        public string Delete(string delIds)
        {
            List<int> listDelIds = Array.ConvertAll<string, int>(delIds.Split(','), m => Falcon.Function.ToInt(m)).ToList();
            Database db = DatabaseFactory.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                foreach (int one in listDelIds)
                {
                    if (new DAL.U_JZX().Delete(one, tran) == 0)
                    {
                        tran.Rollback();
                        return "操作失败！";
                    }
                }
                tran.Commit();
                return "1";
            }
        }

        public string Delete(int id)
        {
            if (new DAL.U_JZX().Delete(id, null) == 1)
            {
                return "1";
            }
            else
            {
                return "操作失败！";
            }
        }
		public int Delete(int id, out string ex)
        {
            if (new DAL.U_JZX().Delete(id, null) == 1)
            {
                ex = "操作成功！";
                return 1;
            }
            else
            {
                ex = "操作失败！";
                return 0;
            }
        }
		
		public bool Delete(params int[] arrDelIds)
		{
			if (arrDelIds.Length == 1)
            {
                return new DAL.U_JZX().Delete(arrDelIds[0], null) == 1;
            }
			Database db = DatabaseFactory.CreateDatabase();
            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                bool flag = true;
                foreach (int one in arrDelIds)
                {
                    if (new DAL.U_JZX().Delete(one, tran) != 1)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            } 
		}
		#endregion
	}
}