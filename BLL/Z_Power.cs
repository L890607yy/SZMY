﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Z_Forms
    {
        #region 添加


        public bool Add(string name, string url, int pid, string power, int sort, int isMenu, int isEnable, string remark)
        {
            return new DAL.Z_Forms().Add(name, url, pid, power, sort, isMenu, isEnable, remark) == 1;
        }
        #endregion

        #region 修改

        public bool Update(int id, string name, string url, int pid, string power, int sort, int isMenu, int isEnable, string remark)
        {
            return new DAL.Z_Forms().Update(id, name, url, pid, power, sort, isMenu, isEnable, remark) == 1;
        }

        #endregion

        #region 查询

        /// <summary>
        /// 获取菜单明细
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.Z_Forms GetModel(int id)
        {
            return new DAL.Z_Forms().GetModel(id);
        }

        //public List<Model.Z_Forms> GetList(int? isEnable)
        //{
        //    return new DAL.Z_Forms().GetList(isEnable);
        //}

        public List<Model.Z_Forms> GetListByWhere(string where)
        {
            return new DAL.Z_Forms().GetListByWhere(where);
        }

        ///// <summary>
        ///// 获取菜单分类列表
        ///// </summary>
        ///// <returns></returns>
        //public List<Model.Z_Forms> GetListModel()
        //{
        //    return new DAL.Z_Forms().GetList(1);
        //}

        private List<Model.Z_Forms> listModel = new List<Model.Z_Forms>();
        public List<Model.Z_Forms> GetSortedList(List<Model.Z_Forms> lists, int pid, int level)
        {
            List<Model.Z_Forms> listTemp = (from m in lists where m.pid == pid select m).ToList();
            if (listTemp != null && listTemp.Count != 0)
            {
                foreach (Model.Z_Forms one in listTemp)
                {
                    one._name = one.name;
                    if (level > 0)
                    {
                        string empty = " ┡";
                        if (level > 1)
                        {
                            for (int i = 1; i < level; i++)
                            {
                                empty += " ┄ "; // " ┄ ┈ ─"
                            }
                        }
                        one._name = empty + one._name;
                    }
                    listModel.Add(one);

                    int child = (from m in lists where m.pid == one.id select m.id).Count();
                    if (child > 0)
                    {
                        GetSortedList(lists, one.id, level + 1);
                    }
                }
            }
            return listModel;
        }

        /// <summary>
        /// 获取dhtmlxTree所需要的json数据
        /// </summary>
        /// <param name="listRecord"></param>
        /// <param name="rootName"></param>
        /// <returns></returns>
        public string GetJsonString(List<Model.Z_Forms> listRecord, string rootName, int role)
        {
            List<Model.PowerInfo> listPowerInfo = new List<Model.PowerInfo>();
            if (role != 0)
            {
                Model.Z_Role modelRole = new BLL.Z_Role().GetModel(role);
                if (modelRole != null && modelRole.id != 0)
                {
                    string power = modelRole.power;
                    if (!string.IsNullOrEmpty(power))
                    {
                        listPowerInfo = Tool.JsonHelper.JsonDeserialize<List<Model.PowerInfo>>(power);
                    }
                }
            }

            Model.dhtmlxTree rootModel = new Model.dhtmlxTree() { id = -1, open = 1, text = rootName };
            Model.dhtmlxTree model = new Model.dhtmlxTree()
            {
                id = 0,
                open = 1,
                text = "根目录",
                item = new List<Model.dhtmlxTree>() { GetJsonModel(listRecord, rootModel, listPowerInfo) }
            };
            string json = Tool.JsonHelper.JsonSerializer<Model.dhtmlxTree>(model);
            return json.Replace("ckchecked", "checked");
        }

        private Model.dhtmlxTree GetJsonModel(List<Model.Z_Forms> listRecord, Model.dhtmlxTree model, List<Model.PowerInfo> listPowerInfo)
        {
            Model.dhtmlxTree modelTree = model;
            List<Model.Z_Forms> listForm = (from m in listRecord where m.pid == (model.id == -1 ? 0 : model.id) orderby m.sort ascending, m.id ascending select m).ToList();
            if (listForm != null && listForm.Count != 0)
            {
                List<Model.dhtmlxTree> listItem = new List<Model.dhtmlxTree>();
                foreach (Model.Z_Forms one in listForm)
                {
                    bool isCheck = false;
                    Model.PowerInfo modelPowerInfo = new Model.PowerInfo();
                    foreach (Model.PowerInfo item in listPowerInfo)
                    {
                        if (item.Fid == one.id)
                        {
                            modelPowerInfo = item;
                            isCheck = true;
                            break;
                        }
                    }

                    int child = (from m in listRecord where m.pid == one.id select m).Count();
                    if (child > 0)
                    {
                        Model.dhtmlxTree temp = new Model.dhtmlxTree() { id = one.id, open = 1, text = one.name, ckchecked = isCheck ? -1 : 0 };
                        listItem.Add(temp);
                        GetJsonModel(listRecord, temp, listPowerInfo);
                    }
                    else
                    {
                        string[] arrayPower = string.IsNullOrEmpty(one.power) ? new string[] { } : one.power.Split(',');
                        List<string> listPower = string.IsNullOrEmpty(modelPowerInfo.Power) ? new List<string>() : modelPowerInfo.Power.Split(',').ToList<string>();
                        StringBuilder str = new StringBuilder();
                        for (int i = 0; i < arrayPower.Length; i++)
                        {
                            string powerName = arrayPower[i];
                            if (!string.IsNullOrEmpty(powerName))
                            {
                                str.Append(" <input type='checkbox' id='ck" + powerName + one.id + "' class='ck" + one.id + "' name='ck' mark='" + one.id + "' value='" + powerName + "|" + one.id + "' " + (listPower.Contains(powerName) ? "checked='checked'" : "") + "/><label for='ck" + powerName + one.id + "'>" + powerName + "</label>  ");
                            }
                        }

                        Model.dhtmlxTree temp = new Model.dhtmlxTree() { id = one.id, open = 1, text = one.name + "&nbsp" + str, ckchecked = isCheck ? 1 : 0 };
                        listItem.Add(temp);
                        if ((from m in listRecord where m.pid == one.id orderby m.sort ascending, m.id ascending select m).Count() != 0)
                        {
                            GetJsonModel(listRecord, temp, listPowerInfo);
                        }
                    }
                    model.item = listItem;
                }
            }
            return modelTree;
        }

        private string GetTaskString(int id, List<Model.Z_Forms> listForms, List<Model.PowerInfo> listPowerInfo)
        {
            List<Model.Z_Forms> listTemp = listForms.Where(m => m.pid == id).ToList();

            if (listTemp.Count == 0) return string.Empty;
            StringBuilder str = new StringBuilder();

            foreach (Model.Z_Forms item in listTemp)
            {
                str.Append("{");
                str.Append("id:" + item.id + ",");
                str.Append("open:1,");

                bool isCheck = false;
                Model.PowerInfo modelPowerInfo = new Model.PowerInfo();
                foreach (Model.PowerInfo one in listPowerInfo)
                {
                    if (one.Fid == item.id)
                    {
                        modelPowerInfo = one;
                        isCheck = true;
                        break;
                    }
                }

                //if (item.child > 0)
                if ((from m in listForms where m.pid == item.id select m).Count() > 0)
                {
                    str.Append("text:\"" + item.name + "\"" + ",");
                    str.Append("checked:" + (isCheck ? "-1" : "0") + ",");
                    str.Append("item:[" + GetTaskString(item.id, listForms, listPowerInfo) + "]},");
                }
                else
                {
                    str.Append("text:\"" + item.name + " &nbsp; ");
                    string[] arrayPower = item.power.Split(',');
                    List<string> listPower = string.IsNullOrEmpty(modelPowerInfo.Power) ? new List<string>() : modelPowerInfo.Power.Split(',').ToList<string>();
                    for (int i = 0; i < arrayPower.Length; i++)
                    {
                        string powerName = arrayPower[i];
                        if (!string.IsNullOrEmpty(powerName))
                        {
                            str.Append("<input type='checkbox' id='ck" + powerName + item.id + "' class='ck" + item.id + "' name='ck' mark='" + item.id + "' value='" + powerName + "|" + item.id + "' " + (listPower.Contains(powerName) ? "checked='checked'" : "") + "/><label for='ck" + powerName + item.id + "'>" + powerName + "</label> &nbsp; ");
                        }
                    }

                    str.Append("\"" + ",checked:" + (isCheck ? "1" : "0") + "},");
                }
            }
            return str.ToString().EndsWith(",") ? str.ToString(0, str.Length - 1) : str.ToString();
        }


        #endregion

        #region 删除

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Delete(int id)
        {
            Model.Z_Forms model = new DAL.Z_Forms().GetModel(id);
            if (model != null && model.id != 0)
            {
                if (new DAL.Z_Forms().GetChildCount(id) == 0)
                {
                    if (new DAL.Z_Forms().Delete(id) == 1)
                    {
                        return "1";
                    }
                    return "操作失败！";
                }
                return "操作失败，当前类型下面有子类！";
            }
            return "操作失败，未找到当前记录！";
        }
        #endregion
    }

    public class Z_Role
    {
        public bool Add(string name, int isEnable, int sort, string remark)
        {
            return new DAL.Z_Role().Add(name, isEnable, sort, remark) == 1;
        }

        public bool Update(int id, string name, int isEnable, int sort, string remark)
        {
            return new DAL.Z_Role().Update(id, name, isEnable, sort, remark) == 1;
        }

        public int SetPower(int id, string power)
        {
            return new DAL.Z_Role().SetPower(id, power);
        }

        public Model.Z_Role GetModel(int id)
        {
            return new DAL.Z_Role().GetModel(id);
        }

        //public List<Model.Z_Role> GetList(int? isEnable)
        //{
        //    return new DAL.Z_Role().GetList(isEnable);
        //}

        public List<Model.Z_Role> GetListByWhere(string where)
        {
            return new DAL.Z_Role().GetListByWhere(where);
        }

        public string Delete(int id)
        {
            //int child = new DAL.Z_Users().GetCountByRole(id);
            //if (child == 0)
            //{
            if (new DAL.Z_Role().Delete(id) == 1)
            {
                return "1";
            }
            return "操作失败！";
            //}
            //ex = "操作失败，原因：当前角色下有系统人员！";
            //return 0;
        }
    }

    public class Z_Users
    {
        public bool Add(string username, string userpswd, string realname, string mobile, string email, int depart, int role, int isEnable, int isDptMgr, string remark, int jbid)
        {
            return new DAL.Z_Users().Add(username, userpswd, realname, mobile, email, depart, role, isEnable, isDptMgr, remark, jbid) == 1;
        }

        public bool Update(int id, string realname, string mobile, string email, int depart, int role, int isEnable, int isDptMgr, string remark, int jbid)
        {
            return new DAL.Z_Users().Update(id, realname, mobile, email, depart, role, isEnable, isDptMgr, remark, jbid) == 1;
        }

        public bool Update(int id, string realname, string mobile, string email)
        {
            return new DAL.Z_Users().Update(id, realname, mobile, email) == 1;
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="oldpswd"></param>
        /// <param name="userpswd"></param>
        /// <returns></returns>
        public bool UpdatePswd(int id, string oldpswd, string userpswd)
        {
            Model.Z_Users model = new DAL.Z_Users().GetModel(id);
            if (model != null && model.id != 0)
            {
                if (Falcon.Function.Encrypt(oldpswd) == model.userpswd)
                {
                    return new DAL.Z_Users().UpdatePswd(id, userpswd) == 1;
                }
                return false;
            }
            return false;
        }

        public bool Update_ResetPswd(int id, string userpswd)
        {
            return new DAL.Z_Users().UpdatePswd(id, userpswd) == 1;
        }

        public Model.Z_Users GetModel(int id)
        {
            return new DAL.Z_Users().GetModel(id);
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="where">isDel=0</param>
        /// <returns></returns>
        public List<Model.Z_Users> GetListByWhere(string where)
        {
            return new DAL.Z_Users().GetListByWhere(where);
        }

        public Model.Z_Users Login(string username)
        {
            return new DAL.Z_Users().Login(username);
        }

        public Model.Z_Users Login(string username, string userpswd)
        {
            return new DAL.Z_Users().Login(username, userpswd);
        }

        public int isExist_UserName(int id, string username)
        {
            return new DAL.Z_Users().isExist_UserName(id, username);
        }

        public string Delete(string delIds)
        {
            List<int> listDelIds = Array.ConvertAll<string, int>(delIds.Split(','), m => Falcon.Function.ToInt(m)).ToList();
            Database db = DatabaseFactory.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                foreach (int one in listDelIds)
                {
                    if (new DAL.Z_Users().Delete(one, tran) == 0)
                    {
                        tran.Rollback();
                        return "操作失败！";
                    }
                }
                tran.Commit();
                return "1";
            }
        }

        public string Delete(int id)
        {
            if (new DAL.Z_Users().Delete(id, null) == 1)
            {
                return "1";
            }
            else
            {
                return "操作失败！";
            }
        }
    }

    public class Z_Depart
    {
        public bool Add(string name, int pid, int sort, string remark)
        {
            return new DAL.Z_Depart().Add(name, pid, sort, remark) == 1;
        }

        public bool Update(int id, string name, int pid, int sort, string remark)
        {
            return new DAL.Z_Depart().Update(id, name, pid, sort, remark) == 1;
        }

        #region 查询

        /// <summary>
        /// 获取部门信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.Z_Depart GetModel(int id)
        {
            return new DAL.Z_Depart().GetModel(id);
        }

        private List<Model.Z_Depart> listModel = new List<Model.Z_Depart>();
        public List<Model.Z_Depart> GetSortedList(List<Model.Z_Depart> lists, int pid, int level)
        {
            if (lists == null || lists.Count == 0) return null;// new List<Model.Z_Depart>();
            List<Model.Z_Depart> listTemp = (from m in lists where m.pid == pid select m).ToList();
            if (listTemp != null && listTemp.Count != 0)
            {
                foreach (Model.Z_Depart one in listTemp)
                {
                    one._name = one.name;
                    if (level > 0)
                    {
                        string empty = " ┡";
                        if (level > 1)
                        {
                            for (int i = 1; i < level; i++)
                            {
                                empty += " ┄ "; // " ┄ ┈ ─"
                            }
                        }
                        one._name = empty + one._name;
                    }
                    listModel.Add(one);

                    int child = (from m in lists where m.pid == one.id select m.id).Count();
                    if (child > 0)
                    {
                        GetSortedList(lists, one.id, level + 1);
                    }
                }
            }
            return listModel;
        }

        public List<Model.Z_Depart> GetListByWhere(string where)
        {
            return new DAL.Z_Depart().GetListByWhere(where);
        }

        #endregion

        #region 删除

        /// <summary>
        /// 删除部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Delete(int id)
        {
            if (new DAL.Z_Depart().GetChildCount(id) == 0)
            {
                if (new DAL.Z_Users().GetCountByWhere("depart=" + id) == 0)
                {
                    if (new DAL.Z_Depart().Delete(id) == 1)
                    {
                        return "1";
                    }
                    return "操作失败！";
                }
                return "无法删除，当前部门下有系统人员！";
            }
            return "无法删除，当前部门下有子部门！";
        }
        #endregion
    }

    public class Z_Settings
    {
        /// <summary>
        /// 0表示失败，非0表示成功
        /// </summary>
        /// <param name="name"></param>
        /// <param name="logo"></param>
        /// <param name="image"></param>
        /// <param name="smtp"></param>
        /// <param name="email"></param>
        /// <param name="pswd"></param>
        /// <returns></returns>
        public int Update(string name, string logo, string image, string smtp, string email, string pswd)
        {
            return new DAL.Z_Settings().Update(name, logo, image, smtp, email, pswd);
        }

        public Model.Z_Settings GetModel_Top()
        {
            return new DAL.Z_Settings().GetModel_Top();
        }
    }

    public class Z_LoginLogs
    {
        public void Add(int uid, string username, string realname, string ip)
        {
            new DAL.Z_LoginLogs().Add(uid, username, realname, ip);
        }

        public string Delete(string delIds)
        {
            List<int> listDelIds = Array.ConvertAll<string, int>(delIds.Split(','), m => Falcon.Function.ToInt(m)).ToList();
            Database db = DatabaseFactory.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                foreach (int one in listDelIds)
                {
                    if (new DAL.Z_LoginLogs().Delete(one, tran) == 0)
                    {
                        tran.Rollback();
                        return "操作失败！";
                    }
                }
                tran.Commit();
                return "1";
            }
        }
    }
}
