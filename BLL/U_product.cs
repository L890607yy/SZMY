﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common; 

namespace BLL{ 
	public class U_product
    {
        #region 增加

        public bool Add(int jzxid, string pbianma, string pxinghao, string pfanhao, string hshui1, string hshui2, int addren, DateTime addtime, DateTime adddate, string erweima, int pdaxid, int pdaid, int xid,DbTransaction tran)
        {
            return new DAL.U_product().Add(jzxid, pbianma, pxinghao, pfanhao, hshui1, hshui2, addren, addtime, adddate, erweima, pdaxid, pdaid, xid, tran) != 0;
        }
        #endregion

        #region 修改

        public bool Update(int id, int jzxid, string pbianma, string pxinghao, string pfanhao, string hshui1, string hshui2, int addren, DateTime addtime, DateTime adddate, string erweima, int pdaxid, int pdaid, int xid,DbTransaction tran)
        {
            return new DAL.U_product().Update(id, jzxid, pbianma, pxinghao, pfanhao, hshui1, hshui2, addren, addtime, adddate, erweima, pdaxid, pdaid, xid, tran) == 1;
        }
        #endregion		
		 
		#region 查询

        public int GetCountBYjzxid(int jzxid, DbTransaction tran)
        {
            return new DAL.U_product().GetCountBYjzxid(jzxid, tran);
        }
		public Model.U_product GetModelByCon(string where,DbTransaction tran)
		{
            return new DAL.U_product().GetModelByCon(where, tran);
		}
		public List<Model.U_product> GetListModel(string where)
		{
		return new DAL.U_product().GetListModel(where);
		}
		public Model.U_product GetModel(int id)
		{
			return new DAL.U_product().GetModel(id);
		}
		#endregion		
		
		#region  删除

        public string Delete(string delIds)
        {
            List<int> listDelIds = Array.ConvertAll<string, int>(delIds.Split(','), m => Falcon.Function.ToInt(m)).ToList();
            Database db = DatabaseFactory.CreateDatabase();
            using (var conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();

                foreach (int one in listDelIds)
                {
                    Model.U_product pm = new BLL.U_product().GetModelByCon("id='"+one+"' ", tran);
                    Model.U_JZX jzxm=new BLL.U_JZX().GetModelByCon(" id='"+pm.jzxid+"' ",tran);
                    if (new DAL.U_product().Delete(one, tran) == 0)
                    {
                        tran.Rollback();
                        return "操作失败！";
                    }
                    else
                    {
                        if (new BLL.U_JZX().Update(jzxm.id, jzxm.name, jzxm.rongliang, jzxm.chuliang - 1, jzxm.riqi, jzxm.addren, jzxm.addtime, jzxm.daynum, jzxm.xiangbian, jzxm.chudate, jzxm.pdaid, jzxm.isFin, jzxm.xid, tran)) { }
                        else
                        {
                            tran.Rollback();
                            return "操作失败！";
                        }
                    }
                }
                tran.Commit();
                return "1";
            }
        }

        public string Delete(int id)
        {
            if (new DAL.U_product().Delete(id, null) == 1)
            {
                return "1";
            }
            else
            {
                return "操作失败！";
            }
        }
		public int Delete(int id, out string ex)
        {
            if (new DAL.U_product().Delete(id, null) == 1)
            {
                ex = "操作成功！";
                return 1;
            }
            else
            {
                ex = "操作失败！";
                return 0;
            }
        }
		
		public bool Delete(params int[] arrDelIds)
		{
			if (arrDelIds.Length == 1)
            {
                return new DAL.U_product().Delete(arrDelIds[0], null) == 1;
            }
			Database db = DatabaseFactory.CreateDatabase();
            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                bool flag = true;
                foreach (int one in arrDelIds)
                {
                    if (new DAL.U_product().Delete(one, tran) != 1)
                    {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            } 
		}
		#endregion
	}
}