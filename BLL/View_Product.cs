﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common; 
using System.Data;
namespace BLL{ 
	public class View_Product
    {
		 
		#region 查询
        public DataTable getDTbyCon(string where)
        {
            return new DAL.View_Product().getDTbyCon(where);
        }

		public List<Model.View_Product> GetListModel(string where)
		{
		return new DAL.View_Product().GetListModel(where);
		}
		#endregion		
		
	}
}