﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Model
{
    public class Z_Depart
    {
        public int id { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 部门名称（带标识）
        /// </summary>
        public string _name { get; set; }

        /// <summary>
        /// 所属父类部门id
        /// </summary>
        public int pid { get; set; }

        /// <summary>
        /// 排序级别
        /// </summary>
        public int sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }

    }

    public class Z_Forms
    {
        public int id { get; set; }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 带标识位的菜单名称
        /// </summary>
        public string _name { get; set; }

        /// <summary>
        /// 菜单地址
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 菜单所属父类的id
        /// </summary>
        public int pid { get; set; }

        /// <summary>
        /// 权限
        /// </summary>
        public string power { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int sort { get; set; }

        /// <summary>
        /// 是否为菜单
        /// </summary>
        public int isMenu { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public int isEnable { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
    }

    public class Z_Role
    {
        public int id { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 角色权限
        /// </summary>
        public string power { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public int isEnable { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }
    }

    public class Z_Users
    {
        public int id { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string username { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string userpswd { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string realname { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string mobile { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// 找回密码链接有效标志位
        /// </summary>
        public int count { get; set; }

        /// <summary>
        /// 所属部门
        /// </summary>
        public int depart { get; set; }

        /// <summary>
        /// 用户角色
        /// </summary>
        public int role { get; set; }

        /// <summary>
        /// 是否启用账号
        /// </summary>
        public int isEnable { get; set; }

        /// <summary>
        /// 是否为部门负责人
        /// </summary>
        public int isDptMgr { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime addTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int sort { get; set; }

        /// <summary>
        /// 当前账号的添加人
        /// </summary>
        public int jbid { get; set; }

        /// <summary>
        /// 当前账号的删除标识
        /// </summary>
        public int isDel { get; set; }

        /// <summary>
        /// 视图，部门名称
        /// </summary>
        public string departname { get; set; }

        /// <summary>
        /// 视图，角色名称
        /// </summary>
        public string rolename { get; set; }

        /// <summary>
        /// 视图，添加人姓名
        /// </summary>
        public string jbname { get; set; }
    }

    public class Z_Settings
    {
        public string name { get; set; }

        public string logo { get; set; }

        public string image { get; set; }

        public string smtp { get; set; }

        public string email { get; set; }

        public string pswd { get; set; }
    }

    public class Z_LoginLogs
    {
        public int id { get; set; }

        public int uid { get; set; }

        public string username { get; set; }

        public string realname { get; set; }

        public string ip { get; set; }

        public DateTime addTime { get; set; }

        public int terminal { get; set; }
    }

    public class PowerInfo
    {
        /// <summary>
        /// 菜单id
        /// </summary>
        public int Fid { get; set; }

        /// <summary>
        /// 菜单地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 权限
        /// </summary>
        public string Power { get; set; }
    }

}