﻿using System; 
using System.Text;
using System.Collections.Generic; 

namespace Model{ 
	public class View_Xmsg
	{ 
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// name
        /// </summary>		
		private string _name;
        public string name
        {
            get{ return _name; }
            set{ _name = value; }
        }        
		/// <summary>
		/// rongliang
        /// </summary>		
		private int _rongliang;
        public int rongliang
        {
            get{ return _rongliang; }
            set{ _rongliang = value; }
        }        
		/// <summary>
		/// chuliang
        /// </summary>		
		private int _chuliang;
        public int chuliang
        {
            get{ return _chuliang; }
            set{ _chuliang = value; }
        }        
		/// <summary>
		/// riqi
        /// </summary>		
		private string _riqi;
        public string riqi
        {
            get{ return _riqi; }
            set{ _riqi = value; }
        }        
		/// <summary>
		/// addren
        /// </summary>		
		private int _addren;
        public int addren
        {
            get{ return _addren; }
            set{ _addren = value; }
        }        
		/// <summary>
		/// addtime
        /// </summary>		
		private DateTime _addtime;
        public DateTime addtime
        {
            get{ return _addtime; }
            set{ _addtime = value; }
        }        
		/// <summary>
		/// daynum
        /// </summary>		
		private int _daynum;
        public int daynum
        {
            get{ return _daynum; }
            set{ _daynum = value; }
        }        
		/// <summary>
		/// xiangbian
        /// </summary>		
		private string _xiangbian;
        public string xiangbian
        {
            get{ return _xiangbian; }
            set{ _xiangbian = value; }
        }        
		/// <summary>
		/// chudate
        /// </summary>		
		private DateTime _chudate;
        public DateTime chudate
        {
            get{ return _chudate; }
            set{ _chudate = value; }
        }        
		/// <summary>
		/// username
        /// </summary>		
		private string _username;
        public string username
        {
            get{ return _username; }
            set{ _username = value; }
        }        
		/// <summary>
		/// realname
        /// </summary>		
		private string _realname;
        public string realname
        {
            get{ return _realname; }
            set{ _realname = value; }
        }        
		 
	}
}