﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class dhtmlxTree
    {
        public int id { get; set; }

        public int open { get; set; }

        public string text { get; set; }

        public int ckchecked { get; set; }//ckchecked为关键字

        public List<dhtmlxTree> item { get; set; }

        public List<UserData> userdata { get; set; }
    }

    public class UserData
    {
        public int sort { get; set; }
    }
}
