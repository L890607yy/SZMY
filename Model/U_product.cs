﻿using System; 
using System.Text;
using System.Collections.Generic; 

namespace Model{ 
	public class U_product
	{ 
      	/// <summary>
		/// id
        /// </summary>		
		private int _id;
        public int id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// jzxid
        /// </summary>		
		private int _jzxid;
        public int jzxid
        {
            get{ return _jzxid; }
            set{ _jzxid = value; }
        }        
		/// <summary>
		/// pbianma
        /// </summary>		
		private string _pbianma;
        public string pbianma
        {
            get{ return _pbianma; }
            set{ _pbianma = value; }
        }        
		/// <summary>
		/// pxinghao
        /// </summary>		
		private string _pxinghao;
        public string pxinghao
        {
            get{ return _pxinghao; }
            set{ _pxinghao = value; }
        }        
		/// <summary>
		/// pfanhao
        /// </summary>		
		private string _pfanhao;
        public string pfanhao
        {
            get{ return _pfanhao; }
            set{ _pfanhao = value; }
        }        
		/// <summary>
		/// hshui1
        /// </summary>		
		private string _hshui1;
        public string hshui1
        {
            get{ return _hshui1; }
            set{ _hshui1 = value; }
        }        
		/// <summary>
		/// hshui2
        /// </summary>		
		private string _hshui2;
        public string hshui2
        {
            get{ return _hshui2; }
            set{ _hshui2 = value; }
        }        
		/// <summary>
		/// addren
        /// </summary>		
		private int _addren;
        public int addren
        {
            get{ return _addren; }
            set{ _addren = value; }
        }        
		/// <summary>
		/// addtime
        /// </summary>		
		private DateTime _addtime;
        public DateTime addtime
        {
            get{ return _addtime; }
            set{ _addtime = value; }
        }        
		/// <summary>
		/// adddate
        /// </summary>		
		private DateTime _adddate;
        public DateTime adddate
        {
            get{ return _adddate; }
            set{ _adddate = value; }
        }        
		/// <summary>
		/// erweima
        /// </summary>		
		private string _erweima;
        public string erweima
        {
            get{ return _erweima; }
            set{ _erweima = value; }
        }        
		/// <summary>
		/// pdaxid
        /// </summary>		
		private int _pdaxid;
        public int pdaxid
        {
            get{ return _pdaxid; }
            set{ _pdaxid = value; }
        }        
		/// <summary>
		/// pdaid
        /// </summary>		
		private int _pdaid;
        public int pdaid
        {
            get{ return _pdaid; }
            set{ _pdaid = value; }
        }        
		/// <summary>
		/// xid
        /// </summary>		
		private int _xid;
        public int xid
        {
            get{ return _xid; }
            set{ _xid = value; }
        }        
		 
	}
}