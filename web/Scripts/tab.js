﻿// isRefresh 是否刷新
function addTabByTitleAndUrl(title, url, tabid, baseTag, isRefresh) {
    // 隐藏二级菜单
    //$(".submenu").hide();
    if (baseTag != null && baseTag != "") {
        url = $("#BasePath_" + baseTag).val() + url;
    }
    openTab(title, url, tabid, isRefresh);
}

//isRefresh 是否刷新
function addTabByTitleAndUrlAndJsonStr(title, url, tabid, baseTag, jsonstr, isRefresh) {
    // 隐藏二级菜单
    //$(".submenu").hide();
    if (baseTag != null && baseTag != "") {
        url = $("#BasePath_" + baseTag).val() + url;
    }
    openTabForJson(title, url, tabid, jsonstr, isRefresh);
}
//isRefresh 是否刷新
function openTabForJson(title, url, tabid, jsonstr, isRefresh) {
    // 如果点击了已打开的tab
    if ($("#" + tabid).length > 0) {
        $("#firstmenu li").each(function () {
            $(this).removeClass("select");
        });

        var index = -1;
        // 移除tab中样式
        $("#tab li").each(function (i) {
            $(this).attr("class", "normal");
            if (this.id == tabid) {
                index = i;
            }
        });
        $("#" + tabid).attr("class", "select");

        if (index != -1) {
            $(".main").hide();
            $($(".main")[index]).show();
            if (isRefresh) {
                $("#" + tabid).attr("url", url);
                $($(".main")[index]).find(".iframe")[0].contentWindow.location.href = url;
                $("#" + tabid + "_jsonStr").val(jsonstr);
            }
        }

    } else {
        // 获取tab的链接

        var html = "";
        if (url.indexOf("?") > -1) {
            html = url + '&tabid=' + tabid;
        } else {
            html = url + '?tabid=' + tabid;
        }

        // 移除tab中样式
        $("#tab li").each(function () {
            $(this).attr("class", "normal");
        });
        var copyHome = $("#home").clone();
        copyHome.attr("id", tabid);
        copyHome.attr("tabid", tabid);
        copyHome.attr("class", "select");
        copyHome.attr("url", html);
        copyHome.html('<input type="hidden" id="' + tabid + "_jsonStr" + '" value="' + jsonstr + '"><span  class="fl" title="'
						+ title
						+ '" url="'
						+ html
						+ '" href="javascript:;" tabid="'
						+ tabid
						+ '" onclick="selectTab(this)">'
						+ title
						+ '</span><a tabid="'
						+ tabid
						+ '" class="tabClose" href="javascript:;" onclick="deleteTab(this)"></a>');
        $("#tab").append(copyHome);

        var copyMainPage = $("#maincontent").clone();
        copyMainPage.addClass("page");
        $(".mainpage").append(copyMainPage);
        $(".main").hide();
        copyMainPage.find(".iframe").attr("src", html);
        copyMainPage.show();

        // 显示tab到
        try {

        } catch (e) { }
    }
}

// 通过菜单新增tab
function addTab(obj, url) {
    var t = $(obj);
    thisurl = t.attr("url");

    if (url) {
        url = url.substring(0, url.lastIndexOf("/") + 1);
        thisurl = url + thisurl;
    }

    var tabid = t.attr("menuid") || t.attr("tabid");

    // 获取tab的标题
    //var title = t.html() == "" ? t.attr("title") : t.text();
    var title = t.attr("title") == undefined ? t.text() : t.attr("title");

    // 隐藏二级菜单
    $(".submenu").hide();
    openTab(title, thisurl, tabid, true);
}
// isRefresh 是否刷新
function openTab(title, url, tabid, isRefresh) {
    url = url.indexOf("?") > -1 ? (url + "&tabid=" + tabid) : (url + "?tabid=" + tabid);

    var random = Math.random();
    // 增加随机数解决 firefox 不能刷新问题 
    url = url + "&random=" + random;
    // 如果点击了已打开的tab

    var t = $("#tab li[tabid='" + tabid + "']");//li

    if (t.length > 0) {
        var index = -1;
        // 移除tab中样式 
        $("#tab li").attr("class", "normal");
        t.attr("class", "select");
        var index = $("#tab li.select").index();
        $(".main").hide();
        $($(".main")[index]).show();
        if (isRefresh) {
            t.attr("url", url);
            $($(".main")[index]).find(".iframe")[0].contentWindow.location.href = url;
        }
    } else {
        // 获取tab的链接 
        //var html = "";
        //if (url.indexOf("?") > -1) {
        //    html = url + '&tabid=' + tabid;
        //} else {
        //    html = url + '?tabid=' + tabid;
        //} 
        // 移除tab中样式 
        $("#tab li").attr("class", "normal");

        var copyHome = $("#home").clone();
        copyHome.attr("id", tabid);
        copyHome.attr("tabid", tabid);
        copyHome.attr("class", "select");
        copyHome.attr("url", url);
        copyHome.html('<span class="fl" onclick="selectTab(this)">' + title + '</span><a class="tabClose" href="javascript:void(0);" onclick="deleteTab(this)"></a>');
        $("#tab").append(copyHome);

        var copyMainPage = $(".main").not(".page").clone();
        copyMainPage.addClass("page");
        $(".mainpage").append(copyMainPage);
        $(".main").hide();
        copyMainPage.find(".iframe").attr("src", url);
        copyMainPage.show();
    }
}

//选中tab
function selectTab(obj, isrefresh) {
    var t = $(obj);
    var title = t.attr("title") || t.text();
    var tabid = t.attr("tabid") || t.parents("li").attr("tabid");
    //tabid = tabid.replace(new RegExp("\\/", "g"), "").replace(".", "").replace("?", "").replace(new RegExp("\\&", "g"), "").replace(new RegExp("\\:", "g"), "").replace(new RegExp("\\=", "g"), "");
    //$("#" + tabid).addClass("select");

    // 移除tab中样式
    var index = 0;
    $("#tab li").each(function (i) {
        $(this).attr("class", "normal");
        if (this.id == tabid) {
            index = i;
        }
    });
    if (tabid == undefined) {
        $("#home").attr("class", "select");
        url = $("#home").attr("url");
        $($(".main")[index]).find(".iframe")[0].contentWindow.location.reload();
    } else {
        $("#" + tabid).attr("class", "select");
    }

    $(".main").hide();
    $($(".main")[index]).show();
    if (isrefresh) {
        $($(".main")[index]).find(".iframe")[0].contentWindow.location.reload();
    }
}

// 删除tab
function deleteTab(obj) {
    var tabid = $(obj).attr("tabid") || $(obj).parents("li").attr("tabid");
    deleteTabByTabId(tabid);
}

function getTabObj(tabid) {
    var j = 0;
    $(window.parent.document).find("#tabDiv li").each(function (i) {
        if ($(this).attr("tabid") == tabid) {
            j = i;
        }
    });
    return $($(window.parent.document).find(".main")[j]).find(".iframe")[0].contentWindow;
}

function deleteTabByTabName(tabname) {
    var title = null;
    var index = -1;
    $("#tab li").each(function (i) {
        title = $(this).find("span").html();
        if (title == tabname) {
            $(this).remove();
            index = i;
        }
    });
    if (index > -1) {
        $($(".main")[index]).remove();
    }
}

// isSelect 表示是否需要选中最后一个tab
function deleteTabByTabId(tabid, isSelect) {
    if (tabid == "" || tabid == null || tabid == undefined) {
        return;
    }
    if ($("#tab li[tabid='" + tabid + "']").attr("locktab")) {
        //selectTab($("#tab li[id='businessprocess-stepone']"));
        showErrorMsg("请先解除锁定后关闭");
        return;
    }
    var index = -1;
    $("#tab li").each(function (i) {
        if (isSelect == undefined || isSelect == null || isSelect) {
            $(this).attr("class", "normal");
        }
        var id = this.id;
        if (id.indexOf(tabid) > -1 && id.substring(id.indexOf(tabid)) == tabid) {
            index = i;
        }
    });
    $("#tab li[tabid='" + tabid + "']").remove();
    if (index > -1) {
        $($(".main")[index]).remove();
    }

    if (isSelect == undefined || isSelect == null || isSelect) {
        //selectTab($("#tab li:last-child")); 
        selectTab($("#tab li:last"));
    }
}

// 给定URL并刷新
function refreshTab(url) {
    if (url != undefined && url != null) {
        $(".main:visible").find(".iframe")[0].contentWindow.location.href = url;
    } else {
        $(".main:visible").find(".iframe")[0].contentWindow.location.reload();
    }
}
// 刷新指定name的tab
function refreshByTabName(tabName) {
    var j = "";
    $("#tab li").each(function (i) {
        var title = $(this).find("span").attr("title");
        if ($.trim(title) == tabName) {
            j = i;
        }
    });
    if (j != "") {
        $(".main").find(".iframe")[j].contentWindow.location.reload();
    }
}


//刷新指定tabid的tab
function refreshByTabId(tabid) {
    var j = "";
    $("#tab li").each(function (i) {
        var curtabid = $(this).find("span").attr("tabid");
        if ($.trim(curtabid) == tabid) {
            j = i;
        }
    });
    if (j != "") {
        $(".main").find(".iframe")[j].contentWindow.location.href = $(".main").find(".iframe")[j].contentWindow.location.href;
    }
}

function refreshAllTab() {
    $("#tab li").each(function (i) {
        $(".main").find(".iframe")[i].contentWindow.location.reload();
    });
}

// 关闭全部tab
function closeAllTab() {
    //var len = $(".main").length;
    $("#tab li").each(function (i) {
        var id = $(this).attr("id");
        //var isremove = true;
        if (id != "home" && !$(this).attr("locktab")) {
            $(this).remove();
        }

        $(".main").not(":first").each(function () {
            if (!$(this).attr("locktab")) {
                $(this).remove();
            }
        });
        $(this).attr("class", "normal");
    });

    $("#home").attr("class", "select");
    $($(".main")[0]).show();

    // 恢复tab行到首页，防止值一个菜单是首页被隐藏
    $(".u .scrol").css({ left: "0px" });
}
// 关闭其他
function closeOtherTab() {
    $("#tab li").each(function (i) {
        var id = $(this).attr("id");
        if (id != "home" && $(this).attr("class") != "select" && !$(this).attr("locktab")) {
            $(this).remove();
        }
    });
    $(".main").each(function (i) {
        if ($(this).hasClass('page') && $(this).css("display") == "none" && !$(this).attr("locktab")) {
            $(this).remove();
        }
    });
    // 恢复tab行到首页，防止值一个菜单是首页被隐藏
    $(".u .scrol").css({ left: "0px" });
}
