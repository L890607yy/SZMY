﻿/// <reference path="jquery-1.11.3.min.js" />

//判断是否跳出框架
if (window.top == window.self) {
    window.location.href = "/login.aspx";
}

////编写序号
//function xuhao(isParent) {
//    var i = 1;
//    if ($(".pagination").length != 0) {
//        var pageindex = Number($(".pagination span.current").not(".prev").not(".next").text());
//        pageindex--;
//        var pagesize = Number($("#pagesize").val());
//        i = pageindex * pagesize;
//        i++;
//    }
//    var rowindex = 0;
//    $(".tbl >tbody>tr", isParent ? parent.document : self.document).not(".total").not(".noquery").not(".tblinfo").each(function () {
//        rowindex++;
//        $(this).addClass(rowindex % 2 == 0 ? "odd" : "");
//        $(this).find("td").not(".ckbox").eq(0).text(i);
//        i++;
//    });
//}

////框架中的页面打开选项卡
//function Tab(obj) {
//    var url = window.location.toString();
//    parent.addTab(obj, url);
//}

function getpower(t) {
    if ($(t).hasClass("no")) {
        layer.msg('您没有此项权限！', { icon: 5, time: 1500 });
        return false;
    }
}

$(function () {

    //var rowindex = 0;
    //$(".tbl tbody tr").each(function () {
    //    rowindex++;
    //    $(this).addClass(rowindex % 2 == 0 ? "odd" : "");
    //});

    ////点击列表中的记录，打开当前记录的明细
    //$(document).on("click", ".tr .info", function () {
    //    $(this).parent().next(".tblinfo").slideToggle(0);
    //});

    ////选中表格中的记录后，高亮切换
    //$(document).on("click", ".tbl>tbody>tr>td:not('.op')", function () {
    //    $(this).parent().toggleClass("current");
    //    $(this).parent().next(".tblinfo").toggle();
    //});


    ////高级搜索
    //$("#gaoji").click(function () {
    //    if ($(".gaoji").is(":hidden")) {
    //        $(".gaoji").show();
    //        $(".ptsearch").hide();
    //        $(this).text("收起高级搜索");
    //    } else {
    //        $(".gaoji").hide();
    //        $(".ptsearch").eq(0).show();
    //        $(this).text("展开高级搜索");
    //    }
    //});

    ////清空搜索条件
    //$("#clearSearch").click(function () {
    //    $(".inputSearch").val("");
    //    var par = $(this).parents("table");
    //    par.find("input:text").val("");
    //    //par.find("select")[0].selectedIndex = 0;这个也可以
    //    par.find("select").each(function () {
    //        $(this).find("option:first").attr("selected", "true");
    //    });
    //});



    //弹窗显示 
    $(document).on("click", ".layer", function () {
        if ($(this).hasClass("no")) {
            layer.msg('您没有此项权限！', { icon: 5, time: 1500 });
        } else {
            var t = $(this);
            var w = t.attr("width");
            var h = t.attr("height");
            var r = parseInt(t.attr("reload"));

            if (!w) w = $(window).width() + "px";
            if (!h) h = $(window).height() + "px";
            if (r == 1) {
                layer.open({
                    type: 2,//0：信息框（默认），1：页面层，2：iframe层，3：加载层，4：tips层。
                    title: t.attr("title"),
                    shadeClose: false,
                    maxmin: true,
                    fix: true,
                    area: [w, h],
                    content: t.attr("href"),
                    end: function (index) {
                        window.location.reload();
                    }
                });
            } else {
                layer.open({
                    type: 2,//0：信息框（默认），1：页面层，2：iframe层，3：加载层，4：tips层。
                    title: t.attr("title"),
                    shadeClose: false,
                    maxmin: true,
                    fix: true,
                    area: [w, h],
                    content: t.attr("href")
                });
            }
        }
        return false;
    });

    //初始化表格控件
    if ($(".fixedTable").length == 1 && $(".fixedTable .noquery").length == 0) {
        var tblHeight = $(".paginator").length == 0 ? 100 : 150;//其他使用方法见demo中的test.html
        $('.fixedTable').fixedHeaderTable({
            width: $(".contentDiv").width() + "px",
            height: ($(window).height() - tblHeight) + "px",
        });
    }
    //全选
    $("#ckDelAll").click(function () {
        $(".ckdel").prop("checked", $(this).prop("checked"));
    });

    //删除，调用后台Delete方法
    $(document).on("click", ".del", function () {
        if ($(this).hasClass("no")) {
            layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
        } else {
            var par = $(this).parents("tr");
            var id = par.attr("id");
            layer.confirm("确实要删除当前记录吗？", function () {
                var res = ajax.Delete(id).value;
                if (res == "1") {
                    layer.msg("操作成功！", { icon: 6, time: 1000 }, function () {
                        par.remove();
                    });
                } else {
                    layer.msg("" + res + "", { icon: 5, time: 1000 });
                }
            });
        }
    });

    //批量删除
    $(".batchdel").click(function () {
        if ($(this).hasClass("no")) {
            layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
        } else {
            var tar_ckbox = $(".ckdel:checked");
            if (tar_ckbox.length == 0) {
                layer.msg("请选择要删除的数据！", { icon: 5, time: 1000 });
                return false;
            }
            var array_ids = [];
            tar_ckbox.each(function () {
                array_ids.push($(this).val());
            });

            layer.confirm("确实要批量删除选中的数据吗？", function () {
                var res = ajax.Delete(array_ids.toString()).value;
                if (res == "1") {
                    layer.msg("操作成功！", { icon: 6, time: 1000 }, function () {
                        tar_ckbox.each(function () {
                            $(this).parents("tr").remove();
                        });
                    });
                } else {
                    layer.msg("" + res + "", { icon: 5, time: 1000 });
                }
            });
        }
    });

    //加载高级查询弹出层---捕获页面
    $(".btnGaoji").click(function () {
        if ($(this).hasClass("no")) {
            layer.msg('您没有此项权限！', { icon: 5, time: 1500 });
            return false;
        }
        layer.open({ //页面层-自定义
            type: 1,
            title: "高级查询",
            closeBtn: 1,
            shadeClose: true,
            area: ["650px", "400px"],
            content: $(".gaoji").html()//捕获页面
        });
    });

    //普通查询操作
    $("#btnSearch").click(function () {
        if ($(this).hasClass("no")) {
            layer.msg('您没有此项权限！', { icon: 5, time: 1500 });
            return false;
        }
        //$(".layui-layer-content .btnGaojiReset").click();//清空高级查询条件
        $(".gaoji table input[type='text']").val("");
        $(".gaoji table select").find("option:first").attr("selected", true);
        $(".gaoji table input[type='checkbox']").prop("checked", $(this).prop("checked"));
    });

    //高级查询操作
    $(document).on("click", ".layui-layer-content .btnGaojiSearch", function () {
        if ($(this).hasClass("no")) {
            layer.msg('您没有此项权限！', { icon: 5, time: 1500 });
            return false;
        }
        $("#txtBase").val("");//清空普通查询条件
        $(".layui-layer-content table input[type='text'],.layui-layer-content table select").each(function () {//同步input type='text'和select的数据
            $(".gaoji table #" + $(this).attr("id")).val($(this).val());
        });
        $(".layui-layer-content table input[type='checkbox']").each(function () {//同步input type='checkbox'的数据
            $(".gaoji table #" + $(this).attr("id")).prop("checked", $(this).prop("checked"));
        });
        $("#btnHiddenSearch").click();
        layer.closeAll();
    });

    //清空高级查询条件
    $(document).on("click", ".btnGaojiReset", function () {
        $(".layui-layer-content table input[type='text']").val("");
        $(".layui-layer-content table select").find("option:first").attr("selected", true);
        $(".layui-layer-content table input[type='checkbox']").prop("checked", false);

        $(".layui-layer-content table input[type='text']").each(function () {//清空input type='text'的数据
            $(".gaoji table #" + $(this).attr("id")).val("");
        });
        $(".layui-layer-content table select").each(function () {//清空select的数据
            $(".gaoji table #" + $(this).attr("id")).find("option:first").attr("selected", true);
        });
        $(".layui-layer-content table input[type='checkbox']").each(function () {//清空input type='checkbox'的数据
            $(".gaoji table #" + $(this).attr("id")).prop("checked", $(this).prop("checked"));
        });
    });

    //listTable选中高亮显示
    $(".listTable>tbody>tr:not('.noquery')").find("td:not('.op')").click(function () {
        $(this).parent().toggleClass("current");
    });

    //自动生成必填项
    $(".tableInfo .required").each(function () {
        var obj = $(this).parents("td").next("td");
        var input = obj.find("input:visible");
        if (input.length == 1) {
            if (!input.attr("datatype")) {
                input.attr("datatype", "*");
            }
        } else {
            var select = obj.find("select");
            if (select.length == 1) {
                if (!select.attr("datatype")) {
                    select.attr("datatype", "*");
                }
            }
        }
    });

    //$(".tipinfo").click(function () {
    //    var tip = $(this).attr("tip");
    //    layer.tips(tip, this, { tips: 1, time: 0, closeBtn: true });
    //});
});
