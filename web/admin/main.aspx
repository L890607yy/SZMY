﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main.aspx.cs" Inherits="web.admin.main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><%=modelSettings.name %></title>
    <link href="/Css/jingyi.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <%--<script src="/Scripts/jingyi.js"></script>--%>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/tab.js"></script>
    <script src="/Scripts/jquery.cookie.js"></script>
    <script type="text/javascript">
        function initSize() {
            $(".iframe").height($(window).height() - $(".top").height() - $("#tabDiv").height() - 10);
        }

        $(function () {
            initSize();
            $(window).resize(function () {
                initSize();
            });

            $(".menu>li").hover(function () {
                $(this).find(".submenu").show();
            }, function () {
                $(this).find(".submenu").hide();
            });


            $(".submenu").width($(".menu").width() - 30);
            $(".submenu:even").css("background", "url(/images/menu_bg1.jpg) no-repeat #FFFFFF right bottom");

            $(".logout").click(function () {
                layer.confirm("确定要退出系统吗？", function () {
                    window.location.href = "/login.aspx";
                });
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="top">
            <div class="topbar">
                <div class="topitem">
                    欢迎登录<%=modelSettings.name %>！当前账号：<%=realname %>，<a class="logout" href="javascript:void(0);">退出登录</a>
                </div>
            </div>
            <div class="menubar">
                <div class="menuitem">
                    <a href="javascript:void(0);">
                        <img class="logo" src="<%=string.IsNullOrEmpty(modelSettings.logo)?"/images/logo_jingyi.png":modelSettings.logo %>" alt="logo" />
                    </a>
                    <ul class="menu">
                        <% 
                            List<Model.Z_Forms> list1 = GetListByPid(listRecord, 0);//获取一级菜单
                            if (list1 != null && list1.Count != 0)
                            {%>
                        <% foreach (Model.Z_Forms one in list1)//循环每个一级菜单
                           {
                               if (isAdmin || listPowersIds.Contains(one.id))
                               {
                        %>
                        <li>
                            <a href="<%=string.IsNullOrEmpty(one.url)?"javascript:void(0);":one.url %>" class="m1"><%=one.name %></a>
                            <% 
                                   List<Model.Z_Forms> list2 = GetListByPid(listRecord, one.id);
                                   if (list2 != null && list2.Count != 0)
                                   {
                            %>
                            <div class="submenu">
                                <% foreach (Model.Z_Forms two in list2)
                                   {
                                       if (isAdmin || listPowersIds.Contains(two.id))
                                       {
                                           List<Model.Z_Forms> list3 = GetListByPid(listRecord, two.id);
                                           bool isRoot = list3 == null || list3.Count == 0 ? false : true;
                                %>
                                <div class="menu2">
                                    <a href="javascript:void(0);" class="<%=isRoot ? "root" : ""%>" menuid="<%=two.id%>" url="/admin/<%=two.url%>" onclick="addTab(this)"><%=two.name%></a>
                                    <% if (isRoot)
                                       { %>
                                    <div class="menu3">
                                        <% foreach (Model.Z_Forms three in list3)
                                           {
                                               if (isAdmin || listPowersIds.Contains(three.id))
                                               { %>
                                        <a href="javascript:void(0);" menuid="<%=two.id%>" url="/admin/<%=two.url%>" fp="1" onclick="addTab(this)"><%=three.name%></a>
                                        <%}
                                           } %>
                                    </div>
                                    <%} %>
                                </div>
                                <%}
                                   } %>
                                <%-- <div class="menu2">
                                    <a href="#" class="root">二级菜单</a>
                                    <div class="menu3" style="width: 310px;">
                                        <a>三级菜单</a>
                                        <a>三级菜单</a>
                                        <a>三级菜单</a>
                                        <a>三级菜单</a>
                                        <a>三级菜单</a>
                                    </div>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>
                                <div class="menu2">
                                    <a href="menu2">二级菜单</a>
                                </div>--%>
                            </div>
                            <%} %>
                        </li>
                        <%}
                           }
                            } %>
                        <%--<li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="submenu">
                                <div class="module_wrap">
                                    <div class="module_hd">
                                        通用解决方案
                                    </div>
                                    <div class="module-bd">
                                        <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a> <a href="">系统设置</a>
                                        <a href="">解决方案管理器</a>
                                    </div>
                                </div>
                            </div>
                        </li>--%>
                        <%-- <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="submenu">
                                完钱而已
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="submenu">
                                的范德萨发犯嘀咕若风退役让他让他同一天人也
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="submenu">
                                范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="submenu">
                                完钱而已非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞范德萨非打死飞
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统设置</a>
                            <div class="submenu">
                                顺丰王瑞峰我热热污染热温热污染
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统订单设置</a>
                            <div class="submenu">
                                完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统订单设置</a>
                            <div class="submenu">
                                完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已
                            </div>
                        </li>
                        <li>
                            <a href="http://www.baidu.com" class="m1">系统订单设置</a>
                            <div class="submenu">
                                完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已完钱而已
                            </div>
                        </li>--%>
                    </ul>
                </div>
            </div>
        </div>
        <div id="tabDiv">
            <div class="tab_title">
                <div class="aleft"></div>
                <div class="u topTab">
                    <ul id="tab" class="scroll">
                        <li id="home" tabid="home" class="select" url="http://www.baidu.com"><span onclick="selectTab(this)">首页</span></li>
                    </ul>
                </div>
                <div id="down" class="downarrow">
                    <div class="tabActionArea">
                        <div id="downlist" class="downArrowContent">
                            <span><a href="javascript:;" onclick="refreshTab()">刷新当前页</a></span>
                            <span><a href="javascript:;" onclick="closeAllTab()">关闭全部</a></span>
                            <span><a href="javascript:;" onclick="closeOtherTab()">关闭其他</a></span>
                        </div>
                    </div>

                </div>
                <div class="aright"></div>
            </div>
        </div>
        <div class="mainpage">
            <div class="main">
                <iframe class="iframe" name="iframe" frameborder="0" scrolling="auto" src="welcome.aspx" style="width: 100%; margin: 0px; padding: 0px;"></iframe>
            </div>
        </div>
    </form>
</body>
</html>
