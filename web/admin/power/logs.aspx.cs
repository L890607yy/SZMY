﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power
{
    public partial class logs : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.Z_LoginLogs> listRecord;

        private static bool flag;

        private void Query()
        {
            int totalCount, pageCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "1=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text.Trim() + "%'";
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text.Trim() + "%'";
                    if (txtIP.Text.Trim() != "") where += " and ip like '%" + txtIP.Text.Trim() + "%'";
                    if (txtStart.Text.Trim() != "") where += " and datediff(day,'" + txtStart.Text.Trim() + "',addTime)>=0";
                    if (txtEnd.Text.Trim() != "") where += "and datediff(day,'" + txtEnd.Text.Trim() + "',addTime)<=0";
                }
            }
            listRecord = Tool.Pager.Query<Model.Z_LoginLogs>(AspNetPager1.CurrentPageIndex, PageSize, out totalCount, out pageCount, "Z_LoginLogs", "*", where, "id desc", 3);
            AspNetPager1.RecordCount = totalCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Delete(string id)
        {
            return new BLL.Z_LoginLogs().Delete(id);
        }

    }
}