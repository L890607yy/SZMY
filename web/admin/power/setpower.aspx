﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="setpower.aspx.cs" Inherits="web.admin.power.setpower" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/dhtmlxTree/codebase/dhtmlxtree.js"></script>
    <link href="/Plug/dhtmlxTree/codebase/dhtmlxtree.css" rel="stylesheet" />
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Scripts/jingyi.js"></script>
    <script type="text/javascript">
        var tree;

        function initTree() {
            $("#div_tree").html("");
            var id = $("#ddlRole").val();
            var jsondata = ajax.GetJsonString(id).value;
            tree = new dhtmlXTreeObject("div_tree", "800px", "100%", 0);
            tree.setImagePath("/Plug/dhtmlxtree/codebase/imgs/csh_vista/");
            tree.enableCheckBoxes(true);
            tree.enableThreeStateCheckboxes(true);//三态树

            tree.setOnCheckHandler(function (id) {
                var allSubItem = tree.getAllSubItems(id);
                if (allSubItem.length == 0) {//如果当前节点没有子节点 
                    $("input:checkbox.ck" + id).prop("checked", tree.isItemChecked(id) == 1);
                } else {
                    var arr = allSubItem.toString().split(',');
                    for (var i = 0; i < arr.length; i++) {
                        $("input:checkbox.ck" + arr[i]).prop("checked", tree.isItemChecked(id) == 1);
                    }
                }
            });

            //从json对象中获取数据生成树 
            tree.parse(JSON.parse(jsondata), "json");
        }

        $(function () {

            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    return false;
                }
            });

            initTree();

            $("#ddlRole").change(function () {
                initTree();
            });

            $("input[name='ck']").click(function () {
                if ($(this).is(":checked")) {
                    tree.setCheck($(this).attr("mark"), 1)//state - checkbox state (0/1/unsure) 
                }
            });


            $("#btnSave").click(function () {
                if ($(this).hasClass("no")) {
                    layer.msg('您没有此项权限！', { icon: 5, time: 1500 });
                    return false;
                }

                var fids = tree.getAllCheckedBranches();
                var role = $("#ddlRole").val();
                var ck = [];
                $("input:checkbox[name='ck']:checked").each(function () {
                    ck.push($(this).val());
                });

                if (ajax.Save(role, fids, ck.toString()).value == 1) {
                    layer.msg('操作成功！', { icon: 6, time: 1500 }, function () {
                        parent.layer.closeAll();
                    });
                } else {
                    layer.msg('操作失败！', { icon: 5, time: 1500 }, function () {
                        parent.layer.closeAll();
                    });
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" id="hideId" value="<%=Falcon.Function.GetQueryInt("id") %>" />
        <div class="main">
            <div class="btnDiv shadow">
                <asp:DropDownList ID="ddlRole" runat="server" CssClass="selectauto"></asp:DropDownList>
                <input type="button" id="btnSave" value="保存" class="btn btnBlue <%=getPower("保存") %>" />
            </div>
            <div class="contentDiv shadow">
                <div id="div_tree">
                </div>
            </div>
        </div>
    </form>
</body>
</html>
