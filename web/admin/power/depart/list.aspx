﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="web.admin.power.depart.list" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <a class="btn btnBlue layer <%=getPower("添加") %>" href="info.aspx" title="添加部门信息">+ 添加</a>
                    <%-- <a class="btn btnRed">批量删除</a>--%>
                </div>
                <%--<div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="name">部门名称</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询")?"":"no" %>" href="javascript:void(0);">高级查询</a>
                    <a class="refresh" href="javascript:window.location.href = window.location;">&nbsp;</a>
                </div>--%>
                <div class="clear"></div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable" style="width: 1500px;">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作</td>
                            <td>部门名称</td>
                            <td>部门负责人</td>
                            <td>排序</td>
                        </tr>
                    </thead>
                    <tbody>
                        <% if (listRecord != null && listRecord.Count != 0)
                           {
                               int i = 0;
                               string update = getPower("修改");
                               string delete = getPower("删除");
                               foreach (Model.Z_Depart one in listRecord)
                               {
                                   List<string> listMgr = (from m in listUsers where m.depart == one.id select m.realname).ToList();
                                   i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%--<input type="checkbox" class="ckdel" value="<%=one.id %>" />--%>
                                <%=i %>
                            </td>
                            <td class="text_center"><a class="label label-primary layer <%=update %>" href="info.aspx?id=<%=one.id %>" title="修改部门信息">修改</a><a class="label label-danger del <%=delete %>">删除</a></td>
                            <td><%=one._name %></td>
                            <td>
                                <%=string.Join(",",listMgr) %>
                            </td>
                            <td class="text_center"><%=one.sort %></td>
                        </tr>
                        <%}
                           }
                           else
                           { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
