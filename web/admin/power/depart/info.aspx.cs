﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power.depart
{
    public partial class info : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);

            if (!IsPostBack)
            {
                List<Model.Z_Depart> lists = new BLL.Z_Depart().GetListByWhere("1=1");
                lists = lists == null ? new List<Model.Z_Depart>() : lists;
                ddlPid.DataSource = new BLL.Z_Depart().GetSortedList(lists, 0, 0);
                ddlPid.DataTextField = "_name";
                ddlPid.DataValueField = "id";
                ddlPid.DataBind();
                ddlPid.Items.Insert(0, new ListItem() { Text = "---顶级部门---", Value = "0" });

                if (id != 0)
                {
                    Model.Z_Depart model = (from m in lists where m.id == id select m).FirstOrDefault();
                    ddlPid.Items.Remove(new ListItem() { Text = model._name, Value = model.id.ToString() });
                    ddlPid.SelectedValue = model.pid.ToString();
                    txtName.Text = model.name;
                    txtSort.Text = model.sort.ToString();
                    txtRemark.Text = model.remark;
                }
            }
        }

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                if (new BLL.Z_Depart().Add(txtName.Text.Trim(), Falcon.Function.ToInt(ddlPid.SelectedValue, 0), Falcon.Function.ToInt(txtSort.Text, 100), txtRemark.Text.Trim()))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                if (new BLL.Z_Depart().Update(id, txtName.Text.Trim(), Falcon.Function.ToInt(ddlPid.SelectedValue, 0), Falcon.Function.ToInt(txtSort.Text, 100), txtRemark.Text.Trim()))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        }
    }
}