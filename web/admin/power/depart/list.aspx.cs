﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power.depart
{
    public partial class list : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                List<Model.Z_Depart> listDeparts = new BLL.Z_Depart().GetListByWhere("1=1");
                listRecord = new BLL.Z_Depart().GetSortedList(listDeparts, 0, 0);

                if (listRecord != null && listRecord.Count != 0)
                {
                    listUsers = new BLL.Z_Users().GetListByWhere("isDptMgr=1");
                }
                listUsers = listUsers == null ? new List<Model.Z_Users>() : listUsers;
            }
        }

        protected List<Model.Z_Depart> listRecord;

        protected List<Model.Z_Users> listUsers;

        [AjaxPro.AjaxMethod]
        public string Delete(int id)
        {
            return new BLL.Z_Depart().Delete(id);
        }
    }
}