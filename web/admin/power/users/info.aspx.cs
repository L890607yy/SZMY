﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power.users
{
    public partial class info : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);

            if (!IsPostBack)
            {
                List<Model.Z_Depart> listDepart = new BLL.Z_Depart().GetListByWhere("1=1");
                listDepart = listDepart == null ? new List<Model.Z_Depart>() : listDepart;
                ddlDepart.DataSource = new BLL.Z_Depart().GetSortedList(listDepart, 0, 0);
                ddlDepart.DataTextField = "_name";
                ddlDepart.DataValueField = "id";
                ddlDepart.DataBind();
                ddlDepart.Items.Insert(0, new ListItem() { Text = "请选择所属部门", Value = "" });

                ddlRole.DataSource = new BLL.Z_Role().GetListByWhere("isEnable=1");
                ddlRole.DataTextField = "name";
                ddlRole.DataValueField = "id";
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem() { Text = "请选择扮演角色", Value = "" });

                if (id != 0)
                {
                    Model.Z_Users model = new BLL.Z_Users().GetModel(id);
                    txtRealName.Text = model.realname;
                    txtUserName.Text = model.username;
                    txtUserName.Enabled = false;
                    txtUserName.Attributes.Remove("ajaxurl");//取消验证用户名重复操作
                    string pswd = Falcon.Function.Decrypt(model.userpswd);
                    txtUserPswd.TextMode = txtUserPswd1.TextMode = TextBoxMode.SingleLine;
                    Regex reg = new Regex("\\w");
                    txtUserPswd.Text = txtUserPswd1.Text = reg.Replace(pswd, "·");
                    txtUserPswd.Enabled = txtUserPswd1.Enabled = false;
                    txtMobile.Text = model.mobile;
                    txtEMail.Text = model.email;
                    ddlDepart.SelectedValue = model.depart.ToString();
                    ckDptMgr.Checked = model.isDptMgr == 1;
                    ddlRole.SelectedValue = model.role.ToString();
                    ckEnable.Checked = model.isEnable == 1;
                    txtRemark.Text = model.remark;
                }
            }
        }

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                if (new BLL.Z_Users().Add(txtUserName.Text, txtUserPswd.Text, txtRealName.Text, txtMobile.Text, txtEMail.Text, Falcon.Function.ToInt(ddlDepart.SelectedValue), Falcon.Function.ToInt(ddlRole.SelectedValue), ckEnable.Checked ? 1 : 0, ckDptMgr.Checked ? 1 : 0, txtRemark.Text, SessionUid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                if (new BLL.Z_Users().Update(id, txtRealName.Text, txtMobile.Text, txtEMail.Text, Falcon.Function.ToInt(ddlDepart.SelectedValue), Falcon.Function.ToInt(ddlRole.SelectedValue), ckEnable.Checked ? 1 : 0, ckDptMgr.Checked ? 1 : 0, txtRemark.Text, SessionUid))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        }
    }
}