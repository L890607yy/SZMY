﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power.users
{
    public partial class list : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                List<Model.Z_Depart> listDepart = new BLL.Z_Depart().GetListByWhere("1=1");
                listDepart = listDepart == null ? new List<Model.Z_Depart>() : listDepart;
                ddlDepart.DataSource = new BLL.Z_Depart().GetSortedList(listDepart, 0, 0);
                ddlDepart.DataTextField = "_name";
                ddlDepart.DataValueField = "id";
                ddlDepart.DataBind();
                ddlDepart.Items.Insert(0, new ListItem() { Text = "--请选择所属部门--", Value = "" });

                ddlRole.DataSource = new BLL.Z_Role().GetListByWhere("isEnable=1");
                ddlRole.DataTextField = "name";
                ddlRole.DataValueField = "id";
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem() { Text = "--请选择扮演角色--", Value = "" });

                flag = false;
                Query();
            }
        }

        protected List<Model.Z_Users> listRecord;

        private static bool flag;

        private void Query()
        {
            int totalCount, pageCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "isDel=0";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtRealName.Text.Trim() != "") where += " and realname like '%" + txtRealName.Text.Trim() + "%'";
                    if (txtUserName.Text.Trim() != "") where += " and username like '%" + txtUserName.Text.Trim() + "%'";
                    if (txtMobile.Text.Trim() != "") where += " and mobile like '%" + txtMobile.Text.Trim() + "%'";
                    if (txtEMail.Text.Trim() != "") where += " and email like '%" + txtEMail.Text.Trim() + "%'";
                    if (ddlDepart.SelectedIndex != 0) where += " and depart=" + ddlDepart.SelectedValue;
                    if (ddlDptMgr.SelectedIndex != 0) where += " and isDptMgr=" + ddlDptMgr.SelectedValue;
                    if (ddlRole.SelectedIndex != 0) where += " and role=" + ddlRole.SelectedValue;
                    if (ddlEnable.SelectedIndex != 0) where += " and isEnable=" + ddlEnable.SelectedValue;
                    if (txtStart.Text.Trim() != "") where += " and datediff(day,'" + txtStart.Text.Trim() + "',addTime)>=0";
                    if (txtEnd.Text.Trim() != "") where += "and datediff(day,'" + txtEnd.Text.Trim() + "',addTime)<=0";
                }
            }
            listRecord = Tool.Pager.Query<Model.Z_Users>(AspNetPager1.CurrentPageIndex, PageSize, out totalCount, out pageCount, "V_Users_Depart_Role", "*", where, "id desc", 3);
            AspNetPager1.RecordCount = totalCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Delete(string id)
        {
            return new BLL.Z_Users().Delete(id);
        }

        [AjaxPro.AjaxMethod]
        public string ResetPswd(int id)
        {
            if (new BLL.Z_Users().Update_ResetPswd(id, "123456"))
            {
                return "已成功将密码重置为：123456";
            }
            else
            {
                return "0";
            }
        }
    }
}