﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="info.aspx.cs" Inherits="web.admin.power.users.info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>真实姓名： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRealName" runat="server" datatype="*" placeholder="请输入真实姓名"></asp:TextBox>
                        <span class="Validform_checktip">请输入真实姓名</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>操作员编号： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserName" runat="server" datatype="*2-15" ajaxurl="/Ajax/checkNameRepeat.ashx" placeholder="请输入操作员编号"></asp:TextBox>
                        <span class="Validform_checktip">请输入操作员编号，长度为5-15位</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>登录密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserPswd" runat="server" datatype="*5-15" TextMode="Password" placeholder="请输入登录密码">123456</asp:TextBox>
                        <span class="Validform_checktip">请输入登录密码，长度为5-15位（默认：123456）</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>确认密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtUserPswd1" runat="server" datatype="*5-15" recheck="txtUserPswd" TextMode="Password" placeholder="请确认登录密码"></asp:TextBox>
                        <span class="Validform_checktip">请确认登录密码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">手机号码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobile" runat="server" datatype="m|/[\d]{7}/" ignore="ignore" placeholder="请输入手机号码"></asp:TextBox>
                        <span class="Validform_checktip">请输入手机号码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">电子邮箱： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtEMail" runat="server" datatype="e" ignore="ignore" placeholder="请输入电子邮箱"></asp:TextBox>
                        <span class="Validform_checktip">请输入电子邮箱，以便找回密码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>所属部门： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDepart" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        <span class="Validform_checktip">请选择当前用户所属的部门</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">部门管理员： 
                    </td>
                    <td>
                        <asp:CheckBox ID="ckDptMgr" runat="server" Text="当前用户为部门管理人员" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>扮演角色： 
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlRole" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                        <span class="Validform_checktip">请选择当前用户扮演的角色</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">状态： 
                    </td>
                    <td>
                        <asp:CheckBox ID="ckEnable" runat="server" Text="启用当前用户" Checked="true" />
                        <span class="Validform_checktip">停用后当前用户无法登录系统</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Style="width: 250px; height: 50px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
