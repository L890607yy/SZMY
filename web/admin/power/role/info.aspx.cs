﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power.role
{
    public partial class info : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);
            if (!IsPostBack)
            {
                if (id != 0)
                {
                    Model.Z_Role model = new BLL.Z_Role().GetModel(id);
                    txtName.Text = model.name;
                    ckEnable.Checked = model.isEnable == 1;
                    txtSort.Text = model.sort.ToString();
                    txtRemark.Text = model.remark;
                }
            }
        }

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (id == 0)
            {
                if (new BLL.Z_Role().Add(txtName.Text.Trim(), ckEnable.Checked ? 1 : 0, Falcon.Function.ToInt(txtSort.Text, 100), txtRemark.Text.Trim()))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                if (new BLL.Z_Role().Update(id, txtName.Text.Trim(), ckEnable.Checked ? 1 : 0, Falcon.Function.ToInt(txtSort.Text, 100), txtRemark.Text.Trim()))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
        }
    }
}