﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power.role
{
    public partial class list : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.Z_Role> listRecord;

        private static bool flag;

        private void Query()
        {
            int totalCount, pageCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "1=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtName.Text.Trim() != "") where += " and name like '%" + txtName.Text.Trim() + "%'";
                    if (ddlEnable.SelectedIndex != 0) where += " and isEnable=" + ddlEnable.SelectedValue;
                }
            }
            listRecord = Tool.Pager.Query<Model.Z_Role>(AspNetPager1.CurrentPageIndex, PageSize, out totalCount, out pageCount, "Z_Role", "*", where, "id asc", 3);
            AspNetPager1.RecordCount = totalCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Delete(int id)
        {
            return new BLL.Z_Role().Delete(id);
        }
    }
}