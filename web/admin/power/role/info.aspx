﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="info.aspx.cs" Inherits="web.admin.power.role.info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>角色名称： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" datatype="*" CssClass="tbox" placeholder="请输入角色名称"></asp:TextBox>
                        <span class="Validform_checktip">请输入角色名称</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">状态： 
                    </td>
                    <td>
                        <asp:CheckBox ID="ckEnable" runat="server" Text="启用当前角色" Checked="true" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl">排序： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtSort" runat="server" datatype="n1-3" placeholder="请输入排序级别" Text="100"></asp:TextBox>
                        <span class="Validform_checktip">请输入0-999之间的数字，数字越小，排序越靠前</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Style="width: 250px; height: 50px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                        <input type="button" id="closeIframe" value="关闭" class="btn btn-sm btn-info" onclick="parent.layer.closeAll()" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
