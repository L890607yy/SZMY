﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.power.forms
{
    public partial class list : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                List<Model.Z_Forms> listForms = new BLL.Z_Forms().GetListByWhere("1=1");
                listRecord = new BLL.Z_Forms().GetSortedList(listForms, 0, 0);
            }
        }

        protected List<Model.Z_Forms> listRecord;

        [AjaxPro.AjaxMethod]
        public string Delete(int id)
        {
            return new BLL.Z_Forms().Delete(id);
        }
    }
}