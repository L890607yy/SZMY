﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="web.admin.power.forms.list" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <a class="btn btnBlue layer <%=getPower("添加") %>" href="info.aspx" title="添加菜单导航">+ 添加</a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable" style="width: 1500px;">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>操作</td>
                            <td>名称</td>
                            <td>地址</td>
                            <td>权限</td>
                            <td>排序</td>
                            <td>菜单项</td>
                            <td>状态</td>
                        </tr>
                    </thead>
                    <tbody>
                        <% if (listRecord != null && listRecord.Count != 0)
                           {
                               int i = 0;
                               string update = getPower("修改");
                               string delete = getPower("删除");
                               foreach (Model.Z_Forms one in listRecord)
                               {
                                   i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td class="text_center"><a class="label label-primary <%=update %> layer" href="info.aspx?id=<%=one.id %>" title="修改菜单导航">修改</a><a class="label label-danger del <%=delete %>">删除</a></td>
                            <td><%=one._name %></td>
                            <td><%=one.url %></td>
                            <td><%=one.power %></td>
                            <td class="text_center"><%=one.sort %></td>
                            <td class="text_center"><%=one.isMenu==1?"✔":"" %></td>
                            <td class="text_center"><%=one.isEnable==1?"✔":"" %></td>
                        </tr>
                        <%}
                           }
                           else
                           { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
