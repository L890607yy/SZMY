﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.jzx
{
    public partial class list : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            List<Model.U_JZX> ljzx = new BLL.U_JZX().GetListModel(" addtime>'" + DateTime.Now.AddDays(-10) + "' ");// (" addtime>'" + DateTime.Now.AddDays(-10) + "' ");

            if (ljzx != null && ljzx.Count != 0)
            {
                string ids = string.Join(",", (from m in ljzx where 1 == 1 select m.id).ToArray());
                List<Model.U_product> lpct = new BLL.U_product().GetListModel(" jzxid in (" + ids + ") ");
                foreach (Model.U_JZX one in ljzx)
                {
                    int count = (from m in lpct where m.jzxid == one.id select m).Count();
                    if (new BLL.U_JZX().Update(one.id, one.name, one.rongliang, count, one.riqi, one.addren, one.addtime, one.daynum, one.xiangbian, one.chudate, one.pdaid, one.isFin, one.xid, null)) { }
                }
            }
            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.View_Xmsg> listRecord;

        private static bool flag;

        private void Query()
        {
            int totalCount, pageCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);

            string where = "1=1";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtName.Text.Trim() != "") where += " and name like '%" + txtName.Text.Trim() + "%'";
                    if (txtRongliang.Text.Trim() != "") where += " and rongliang like '%" + txtRongliang.Text.Trim() + "%'";
                    if (txtBianhao.Text.Trim() != "") where += " and xiangbian like '%" + txtBianhao.Text.Trim() + "%'";
                    if (txtShuliang.Text.Trim() != "") where += " and chuliang like '%" + txtShuliang.Text.Trim() + "%'";
                    if (txtCStart.Text.Trim() != "") where += " and datediff(day,'" + txtCStart.Text.Trim() + "',chudate)>=0";
                    if (txtCEnd.Text.Trim() != "") where += "and datediff(day,'" + txtCEnd.Text.Trim() + "',chudate)<=0";

                    if (txtStart.Text.Trim() != "") where += " and datediff(day,'" + txtStart.Text.Trim() + "',riqi)>=0";
                    if (txtEnd.Text.Trim() != "") where += "and datediff(day,'" + txtEnd.Text.Trim() + "',riqi)<=0";
                }
            }
            listRecord = Tool.Pager.Query<Model.View_Xmsg>(AspNetPager1.CurrentPageIndex, PageSize, out totalCount, out pageCount, "View_Xmsg", "*", where, "id desc", 3);
            AspNetPager1.RecordCount = totalCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Delete(string id)
        {
            return new BLL.U_JZX().Delete(id);
        }

    }
}