﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.jzx
{
    public partial class info : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            id = Falcon.Function.GetQueryInt("id", 0);

            if (!IsPostBack)
            {
                if (id != 0)
                {
                    Model.U_JZX model = new BLL.U_JZX().GetModel(id);
                    txtName.Text = model.name;
                    txtRong.Text = model.rongliang + "";
                }
            }
        }

        protected int id;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int Nxlh = (new BLL.U_JZX().GetMaxIDAtToday(NDate, null)) + 1;
            if (id == 0)
            {

                if (new BLL.U_JZX().Add(txtName.Text.Trim(), ToInt(txtRong.Text.Trim()), 0, NDate, SessionUid, DateTime.Now, Nxlh, DateTime.Now.ToString("yyyyMMdd") + Nxlh.ToString("00"), Config.defConfig.defDate, SessionUid, 0, 0, null))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                }
            }
            else
            {
                Model.U_JZX dm = new BLL.U_JZX().GetModel(id);
                if (dm != null && dm.id != 0)
                {
                    if (new BLL.U_JZX().Update(id, txtName.Text.Trim(), ToInt(txtRong.Text.Trim()), dm.chuliang, dm.riqi, dm.addren, dm.addtime, dm.daynum, dm.xiangbian, dm.chudate, dm.pdaid, dm.isFin, dm.xid, null))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
                else
                {
                    if (new BLL.U_JZX().Add(txtName.Text.Trim(), ToInt(txtRong.Text.Trim()), 0, NDate, SessionUid, DateTime.Now, Nxlh, DateTime.Now.ToString("yyyyMMdd") + Nxlh.ToString("00"), Config.defConfig.defDate, SessionUid, 0, 0, null))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
                    }
                }
            }
        }
    }
}