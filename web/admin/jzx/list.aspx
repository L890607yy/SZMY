﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="web.admin.jzx.list" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/FixedHeaderTable/css/defaultTheme.css" rel="stylesheet" />
    <script src="/Plug/FixedHeaderTable/jquery.fixedheadertable.js"></script>
    <script src="/Plug/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link href="/Plug/My97DatePicker/skin/WdatePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $(".resetpswd").click(function () {
                if ($(this).hasClass("no")) {
                    layer.msg("您没有此项权限！", { icon: 5, time: 1000 });
                    return false;
                }

                var id = $(this).parents("tr").attr("id");
                layer.confirm("确定要重置当前用户的登录密码吗？", function () {
                    var res = ajax.ResetPswd(id).value;
                    if (res == 0) {
                        layer.msg("操作失败！", { icon: 5, time: 1000 });
                    } else {
                        layer.msg("" + res + "", { icon: 6, time: 1500 });
                    }
                });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="btnDiv shadow">
                <div class="f_l">
                    <%--<a class="btn btnBlue layer <%=getPower("添加") %>" href="info.aspx" title="添加系统人员">+ 添加</a>--%>
                    <a class="btn btnRed batchdel" <%=getPower("批量删除") %>>批量删除</a>
                </div>
                <div class="f_r">
                    <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control auto">
                        <asp:ListItem Value="name">箱名称</asp:ListItem>
                        <asp:ListItem Value="xiangbian">箱编号</asp:ListItem>
                        <asp:ListItem Value="rongliang">箱容量</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtBase" runat="server" CssClass="inputSearch"></asp:TextBox>
                    <asp:Button ID="btnSearch" runat="server" CssClass="btn btnBlue" Text="查询" OnClick="btnSearch_Click" />
                    <asp:Button ID="btnHiddenSearch" runat="server" Text="高级查询" CssClass="hidden" OnClick="btnHiddenSearch_Click" />
                    <a class="btn btnGaoji <%=getPower("高级查询") %>" href="javascript:void(0);">高级查询</a>
                    <a class="refresh" href="javascript:window.location.href = window.location;">&nbsp;</a>
                </div>
                <div class="clear"></div>
            </div>
            <div class="gaoji hidden">
                <div class="areagaoji">
                    <table class="tblgaoji">
                        <tr>
                            <td class="tdl">箱名称：</td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">箱容量：</td>
                            <td>
                                <asp:TextBox ID="txtRongliang" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">箱编号：</td>
                            <td>
                                <asp:TextBox ID="txtBianhao" runat="server"></asp:TextBox>
                            </td>
                            <td class="tdl">产品数量：</td>
                            <td>
                                <asp:TextBox ID="txtShuliang" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">创建日期：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdl">出厂日期：</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtCStart" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                                <asp:TextBox ID="txtCEnd" runat="server" CssClass="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <div class="btns">
                        <input type="button" class="btn btnBlue btnGaojiSearch" value="搜索" onclick="return false;" />
                        <input type="reset" class="btn btnGaojiReset" />
                    </div>
                </div>
            </div>
            <div class="contentDiv shadow">
                <table class="listTable fixedTable" style="width: 1800px;">
                    <thead>
                        <tr class="text_center">
                            <td>序号
                            </td>
                            <td>
                                <asp:CheckBox ID="ckDelAll" runat="server" Text="删除" />
                            </td>
                            <td>操作</td>
                            <td>名称</td>
                            <td>箱编号</td>
                            <td>容纳数量</td>
                            <td>产品数量</td>
                            <td>创建人</td>
                            <td>创建日期</td>
                            <td>出厂日期</td>
                        </tr>
                    </thead>
                    <tbody class="text_center">
                        <% if (listRecord != null && listRecord.Count != 0)
                           {
                               int i = (AspNetPager1.CurrentPageIndex - 1) * AspNetPager1.PageSize;
                               string update = getPower("修改");
                               string resetpswd = getPower("重置密码");
                               string delete = getPower("删除");
                               foreach (Model.View_Xmsg one in listRecord)
                               {
                                   i++;
                        %>
                        <tr id="<%=one.id %>" class="<%=i%2==0?"odd":"" %>">
                            <td class="text_center">
                                <%=i %>
                            </td>
                            <td class="text_center op">
                                <input type="checkbox" class="ckdel" value="<%=one.id %>" />
                            </td>
                            <td class="text_center op">
                                <a class="label label-primary <%=update %> layer" href="info.aspx?id=<%=one.id %>" title="修改箱信息">修改</a>
                                <a class="label label-danger del <%=delete %>">删除</a></td>
                            <td class="text_center">
                                <%=one.name %> 
                            </td>
                            <td>
                                <%=one.xiangbian %> 
                            </td>
                            <td>
                                <%=one.rongliang %> 
                            </td>
                            <td>
                                <a href="../product/list.aspx?xid=<%=one.id %>" class="label label-info <%=update %> layer" title="<%=one.name+" 已装载产品管理" %>"><%=one.chuliang %></a>
                            </td>
                            <td>
                                <%=one.realname %>
                            </td>
                            <td>
                                <%=one.addtime %> 
                            </td>
                            <td>
                                <%=one.chudate %>
                            </td>
                        </tr>
                        <%}
                           }
                           else
                           { %>
                        <tr class="noquery ">
                            <td colspan="100">
                                <img src="/images/no_query.png" />
                            </td>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
                <div class="paginator clearfix">
                    <div class="pager">
                        <webdiyer:AspNetPager ID="AspNetPager1" runat="server" CssClass="pagination pagination-sm" AlwaysShow="true" LayoutType="Ul" PagingButtonLayoutType="UnorderedList" PagingButtonSpacing="0" CurrentPageButtonClass="active" CurrentPageButtonPosition="Center" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页" PrevPageText="上一页" OnPageChanged="AspNetPager1_PageChanged">
                        </webdiyer:AspNetPager>
                        <div class="pagerinfo">
                            <asp:Label ID="lblInfo1" runat="server"></asp:Label>
                            共 <%=AspNetPager1.RecordCount %> 条数据，当前页 <%=AspNetPager1.CurrentPageIndex %> / <%=AspNetPager1.PageCount %>，每页 
                        <asp:TextBox ID="txtPageSize" runat="server" Text="20"></asp:TextBox>
                            条数据
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
