﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="individual.aspx.cs" Inherits="web.admin.settings.individual" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>真实姓名： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtRealName" runat="server" datatype="*" placeholder="请输入真实姓名"></asp:TextBox>
                        <span class="Validform_checktip">请输入真实姓名</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>登录账号： 
                    </td>
                    <td>
                        <asp:Label ID="txtUserName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">联系方式： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtMobile" runat="server" datatype="m" ignore="ignore" placeholder="请输入联系方式"></asp:TextBox>
                        <span class="Validform_checktip">请输入联系方式</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">电子邮箱： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtEMail" runat="server" datatype="e" ignore="ignore" placeholder="请输入电子邮箱"></asp:TextBox>
                        <span class="Validform_checktip">请输入电子邮箱，以便找回密码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>所属部门： 
                    </td>
                    <td>
                        <asp:Label ID="txtDepart" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">部门管理员： 
                    </td>
                    <td>
                        <asp:CheckBox ID="ckDptMgr" runat="server" Text="当前用户为部门管理人员" />
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>扮演角色： 
                    </td>
                    <td>
                        <asp:Label ID="txtRole" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">状态： 
                    </td>
                    <td>
                        <asp:CheckBox ID="ckEnable" runat="server" Text="启用当前用户" Checked="true" />
                        <span class="Validform_checktip">停用后当前用户无法登录系统</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">备注： 
                    </td>
                    <td>
                        <asp:Label ID="txtRemark" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>