﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uppswd.aspx.cs" Inherits="web.admin.settings.uppswd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <script type="text/javascript">
        $(function () {
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>原始密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtOldPswd" runat="server" datatype="*5-15" CssClass="tbox" TextMode="Password" placeholder="请输入原始密码"></asp:TextBox>
                        <span class="Validform_checktip">请输入原始密码</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>新密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewPswd" runat="server" datatype="*5-15" CssClass="tbox" TextMode="Password" placeholder="请输入新密码"></asp:TextBox>
                        <span class="Validform_checktip">请输入新密码，长度为5-15位</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl"><span class="required">*</span>确认新密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtNewPswd2" runat="server" datatype="*5-15" recheck="txtNewPswd" CssClass="tbox" TextMode="Password" placeholder="请确认新密码" errormsg="两次输入的密码不一致！"></asp:TextBox>
                        <span class="Validform_checktip">请确认新密码</span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
