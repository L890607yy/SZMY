﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="settings.aspx.cs" Inherits="web.admin.settings.settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/Css/common.css" rel="stylesheet" />
    <script src="/Scripts/jquery-1.11.3.min.js"></script>
    <script src="/Plug/layer-v2.1/layer/layer.js"></script>
    <script src="/Scripts/jingyi.js"></script>
    <link href="/Plug/Validform/validate.css" rel="stylesheet" />
    <script src="/Plug/Validform/Validform_v5.3.2.js"></script>
    <link href="/Plug/kindeditor-4.1.10/themes/default/default.css" rel="stylesheet" />
    <script src="/Plug/kindeditor-4.1.10/kindeditor.js"></script>
    <script src="/Plug/kindeditor-4.1.10/lang/zh_CN.js"></script>
    <script type="text/javascript">
        KindEditor.ready(function (K) {
            var editor = K.editor({
                allowFileManager: true,
                uploadJson: '/Ajax/upload_json.ashx?type=1&size=0.5',
                //fileManagerJson: '/Ajax/file_manager_json.ashx',
            });
            K('#uplogo').click(function () {
                editor.loadPlugin('image', function () {
                    editor.plugin.imageDialog({
                        showRemote: false,
                        //imageUrl: K('#imgLogo').attr("src"),
                        clickFn: function (url, title, width, height, border, align) {
                            K('#imgLogo').attr("src", url)
                            editor.hideDialog();
                            imgUrl = url;
                        }
                    });
                });
            });

            K('#upimage').click(function () {
                editor.loadPlugin('image', function () {
                    editor.plugin.imageDialog({
                        showRemote: false,
                        //imageUrl: K('#imgLogo').attr("src"),
                        clickFn: function (url, title, width, height, border, align) {
                            K('#imgImage').attr("src", url)
                            editor.hideDialog();
                            imageUrl = url;
                        }
                    });
                });
            });
        });

        var imgPath, imgUrl;
        var imagePath, imageUrl;

        $(function () {
            //$("#imgLogo").attr("src", "http://kindeditor.net/images/logo.png");
            imgPath = $("#imgLogo").attr("src");
            imagePath = $("#imgImage").attr("src");
            $("form").Validform({
                tiptype: 3,
                label: ".tdl",
                showAllError: true,
                beforeSubmit: function (curform) {
                    if ($("#btnSave").hasClass("no")) {
                        layer.msg('您没有此项权限！', { icon: 5, time: 1500 });
                        return false;
                    }

                    //在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
                    //这里明确return false的话表单将不会提交;
                    var name = $("#txtName").val();
                    var imgLogo = $("#imgLogo").attr("src");
                    var imageLogo = $("#imgImage").attr("src");
                    var smtp = $("#txtSmtp").val();
                    var email = $("#txtEmail").val();
                    var pswd = $("#txtPswd").val();

                    if (ajax.Save(name, imgLogo, imageLogo, smtp, email, pswd).value == 0) {
                        layer.msg("操作失败，请重试！", { icon: 5, time: 1500 });
                    } else {
                        layer.msg("操作成功！", { icon: 6, time: 1000 }, function () {
                            if (imgPath != imgUrl) {
                                ajax.DeleteFile(imgPath);
                            }
                            if (imagePath != imageUrl) {
                                ajax.DeleteFile(imagePath);
                            }
                        });
                    }
                    return false;
                }
            });
        });
    </script>
</head>
<body class="white">
    <form id="form1" runat="server">
        <div class="main">
            <table class="tableInfo">
                <tr>
                    <td class="tdl" style="width: 200px;"><span class="required">*</span>系统名称： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" datatype="*" placeholder="请输入系统名称"></asp:TextBox>
                        <span class="Validform_checktip">请输入系统名称</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">系统logo： 
                    </td>
                    <td>
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="/images/logo_jingyi.png" />
                        <span class="Validform_checktip">logo宽高度为150*50像素，建议png透明格式</span>
                        <div style="margin: 10px 3px;">
                            <span id="uplogo" class="label label-success">更换logo</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">登录页图片： 
                    </td>
                    <td>
                        <asp:Image ID="imgImage" runat="server" ImageUrl="/images/logo_login.jpg" Style="width: 250px" />
                        <span class="Validform_checktip">图片宽高度为550*360像素，建议png透明格式</span>
                        <div style="margin: 10px 3px;">
                            <span id="upimage" class="label label-success">更换图片</span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">smtp服务器： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtSmtp" runat="server" CssClass="tbox" placeholder="请输入smtp协议"></asp:TextBox>
                        <span class="Validform_checktip">请输入smtp服务器地址，如smtp.163.com，smtp.sd.chinamobile.com等</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">电子邮箱： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtEmail" runat="server" datatype="e" ignore="ignore" placeholder="请输入电子邮箱"></asp:TextBox>
                        <span class="Validform_checktip">用于找回密码及发送邮件，邮箱必须支持smtp协议，建议使用163邮箱。没有邮箱？<a href="http://reg.email.163.com/unireg/call.do?cmd=register.entrance&from=163mail" target="_blank">点此申请</a></span>
                    </td>
                </tr>
                <tr>
                    <td class="tdl">邮箱密码： 
                    </td>
                    <td>
                        <asp:TextBox ID="txtPswd" runat="server" CssClass="tbox" placeholder="请输入邮箱密码"></asp:TextBox>
                        <span class="Validform_checktip">请输入邮箱密码</span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-sm btnBlue" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
