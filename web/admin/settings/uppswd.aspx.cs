﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class uppswd : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSave.CssClass += getPower("保存");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (new BLL.Z_Users().UpdatePswd(SessionUid, txtOldPswd.Text.Trim(), txtNewPswd.Text.Trim()))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功，请牢记新密码！',{icon:6,time:1500});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }
    }
}