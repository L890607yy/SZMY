﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin.settings
{
    public partial class individual : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            if (!IsPostBack)
            {
                btnSave.CssClass += getPower("保存");

                Model.Z_Users model = new BLL.Z_Users().GetModel(SessionUid);
                txtRealName.Text = model.realname;
                txtUserName.Text = model.username;
                txtMobile.Text = model.mobile;
                txtEMail.Text = model.email;
                txtDepart.Text = model.departname;
                ckDptMgr.Checked = model.isDptMgr == 1;
                txtRole.Text = model.rolename;
                ckEnable.Checked = model.isEnable == 1;
                ckDptMgr.Enabled = ckEnable.Enabled = false;
                txtRemark.Text = model.remark;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (new BLL.Z_Users().Update(SessionUid, txtRealName.Text, txtMobile.Text, txtEMail.Text))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作成功！',{icon:6,time:1000});", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('操作失败！',{icon:5,time:1000});", true);
            }
        }
    }
}