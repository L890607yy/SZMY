﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;

namespace web.admin.product
{
    public partial class list : Power
    {
        protected int xid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            xid = Falcon.Function.GetQueryInt("xid", 0);
            if (!IsPostBack)
            {
                btnSearch.CssClass += getPower("查询");

                flag = false;
                Query();
            }
        }

        protected List<Model.View_Product> listRecord;

        private static bool flag;

        private void Query()
        {
            int totalCount, pageCount;
            int PageSize = Falcon.Function.ToInt(txtPageSize.Text, 10);
            string where = "jzxid='"+xid+"' ";
            if (flag)
            {
                if (txtBase.Text.Trim() != "")
                {
                    where += " and " + ddlFilter.SelectedValue + " like '%" + txtBase.Text.Trim() + "%'";
                }
                else
                {
                    if (txtBianma.Text.Trim() != "") where += " and pbianma like '%" + txtBianma.Text.Trim() + "%'";
                    if (txtXinghao.Text.Trim() != "") where += " and pxinghao like '%" + txtXinghao.Text.Trim() + "%'";
                    if (txtFanhao.Text.Trim() != "") where += " and pfanhao like '%" + txtFanhao.Text.Trim() + "%'";
                    if (txtDate.Text.Trim() != "") where += " and adddate like '%" + txtDate.Text.Trim() + "%'";
                }
            }
            listRecord = Tool.Pager.Query<Model.View_Product>(AspNetPager1.CurrentPageIndex, PageSize, out totalCount, out pageCount, "View_Product", "*", where, "id desc", 3);
            AspNetPager1.RecordCount = totalCount;
            AspNetPager1.PageSize = PageSize;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void btnHiddenSearch_Click(object sender, EventArgs e)
        {
            AspNetPager1.CurrentPageIndex = 1;
            flag = true;
            Query();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            Query();
        }

        [AjaxPro.AjaxMethod]
        public string Delete(string id)
        {
            return new BLL.U_product().Delete(id);
        }

        protected void btnDC_Click(object sender, EventArgs e)
        {
            string where = "jzxid='" + xid + "' ";
            Model.U_JZX xm = new BLL.U_JZX().GetModel(xid);
            DataTable dt = new BLL.View_Product().getDTbyCon(where);
            List<Model.Z_Users> luser = new BLL.Z_Users().GetListByWhere("");
            dt.Columns.Add("csv");
            string csvstr = "";
            foreach(DataRow one in dt.Rows)
            {
                string czyid = "00";
                Model.Z_Users um = (from m in luser where m.id == ToInt(one["addren"].ToString()) select m).FirstOrDefault();
                if (um != null && um.id != 0) { czyid = um.username; }
                csvstr += ("\"" + one["erweima"].ToString() + "\",\"2\",\"MHL\", ,\"" + ToInt(one["hshui1"].ToString()).ToString("00") + "\",\"" + ToInt(one["hshui2"].ToString()).ToString("00") + "\",\"" + czyid + "\",\"" + DateTime.Parse(one["addtime"].ToString()).ToString("yyyyMMddHHmmss") + "\",\"OUT\", ,\"" + DateTime.Parse(one["addtime"].ToString()).ToString("yyyy/MM/dd") + "\"\r\n");
                
                one["csv"] = "\"" + one["erweima"].ToString() + "\",\"2\",\"MHL\", ,\"" + ToInt(one["hshui1"].ToString()).ToString("00") + "\",\"" + ToInt(one["hshui2"].ToString()).ToString("00") + "\",\"" + czyid + "\",\"" + DateTime.Parse(one["addtime"].ToString()).ToString("yyyyMMddHHmmss") + "\",\"OUT\", ,\"" + DateTime.Parse(one["addtime"].ToString()).ToString("yyyy/MM/dd") + "\"";
            }
            //在将文本写入文件前，处理文本行
            //StreamWriter一个参数默认覆盖
            //StreamWriter第二个参数为false覆盖现有文件，为true则把文本追加到文件末尾
            string fileName = xm.name + "" + xm.xiangbian + "" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            string path = @"d:\save\";
            
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            fileName =(path+ System.Web.HttpUtility.UrlEncode(fileName));
            SaveFile(csvstr, fileName);
            FileInfo fileInfo = new FileInfo(fileName);
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);
            Response.AddHeader("Content-Length", fileInfo.Length.ToString());
            Response.AddHeader("Content-Transfer-Encoding", "binary");
            Response.ContentType = "application/octet-stream";
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("gb2312");
            Response.WriteFile(fileInfo.FullName);
            Response.Flush();
            Response.End();
            if (File.Exists(fileName)) { File.Delete(fileName); }
            //ClientScript.RegisterStartupScript(this.GetType(), "", "layer.alert('文件生成成功！位于：" + fileName + "', {icon: 6,skin: 'layer-ext-moon' }),function(){parent.location.reload();});", true);
            ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('文件生成成功',{icon:6,time:1000},function(){parent.location.reload();parent.layer.closeAll();});", true);
            //layer.alert('文件生成成功！位于：" + fileName + "', {icon: 6,skin: 'layer-ext-moon' })
            //Tool.DataTable2Excel.CreateExcel("", "三泽木业[" + xm.name + "-" + xm.xiangbian + "]数据导出", dt, new string[] { "操作员代码,addren", "产品编码,pbianma", "产品型号,pxinghao", "产品番号,pfanhao", "含水率,hshui1", "含水率,hshui2", "出荷时间,addtime", "出荷日期,adddate", "信息,csv"});
        }
        public static void SaveFile(string p_Text, string p_Path)
        {
            System.IO.StreamWriter _StreamWriter = new System.IO.StreamWriter(p_Path, false, Encoding.GetEncoding("gb2312"));
            _StreamWriter.Write(p_Text);
            _StreamWriter.Close();
        } 
    }
}