﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin
{
    public partial class main : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkLogin();

            isAdmin = false;
            if (SessionRole != 0) //如果不是超级管理员
            {
                string power = new BLL.Z_Role().GetModel(SessionRole).power;
                List<Model.PowerInfo> listPower = Tool.JsonHelper.JsonDeserialize<List<Model.PowerInfo>>(power);
                listPowersIds = new List<int>();
                if (listPower != null && listPower.Count != 0)
                {
                    listPower.ForEach(m => listPowersIds.Add(m.Fid));
                }
            }
            else if (SessionUid != 0)
            {
                isAdmin = true;
            }

            listRecord = new BLL.Z_Forms().GetListByWhere("isEnable=1 and isMenu=1");

            modelSettings = new BLL.Z_Settings().GetModel_Top();
            modelSettings = modelSettings == null ? new Model.Z_Settings() : modelSettings;

            realname = Request.Cookies["userinfo"] == null ? new BLL.Z_Users().GetModel(SessionUid).realname : Server.UrlDecode(Request.Cookies["userinfo"]["name"]);
        }

        protected bool isAdmin;

        protected string realname;

        protected List<int> listPowersIds;

        protected List<Model.Z_Forms> listRecord;

        protected Model.Z_Settings modelSettings;

        protected List<Model.Z_Forms> GetListByPid(List<Model.Z_Forms> listRecord, int pid)
        {
            return (from m in listRecord where m.pid == pid orderby m.sort ascending, m.id ascending select m).ToList();
        }
    }
}