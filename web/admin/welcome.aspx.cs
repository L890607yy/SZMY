﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web.admin
{
    public partial class welcome : System.Web.UI.Page
    {
        private double GetKHSC(DateTime t1, DateTime t2)
        {
            double sc = 0;
            if (t1.Date == t2.Date)
            {
                DateTime t3 = new DateTime();
                DateTime t4 = new DateTime();
                if (t1.Hour < 6) { t3 = DateTime.Parse(t1.Date.ToString("yyyy-MM-dd") + " 06:00:00"); } else { t3 = t1; }
                if (t2.Hour < 6) { t4 = DateTime.Parse(t2.Date.ToString("yyyy-MM-dd") + " 06:00:00"); } else { t4 = t2; }
                sc = (t4 - t3).TotalMinutes;
            }
            else
            {
                int days = (t2 - t1).Days;

                DateTime t3 = new DateTime();
                DateTime t4 = new DateTime();
                if (t1.Hour < 6) { t3 = DateTime.Parse(t1.Date.ToString("yyyy-MM-dd") + " 06:00:00"); } else { t3 = t1; }
                if (t2.Hour < 6) { t4 = DateTime.Parse(t2.Date.ToString("yyyy-MM-dd") + " 06:00:00"); } else { t4 = t2; }

                double min1 = (DateTime.Parse(t1.Date.ToString("yyyy-MM-dd") + " 23:59:59") - t3).TotalMinutes;
                double min2 = (t4 - DateTime.Parse(t2.Date.ToString("yyyy-MM-dd") + " 06:00:00")).TotalMinutes;

                sc = ((t2 - t1).TotalDays>1?(days * 18 * 60):0) + min1 + min2;
            }
            return sc;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime t1 = DateTime.Parse("2016-03-01 19:45:39.000");
            DateTime t2 = DateTime.Parse("2016-03-02 09:13:06.000");
            double gzsc = (t2 - t1).TotalMinutes;
            double khsc = GetKHSC(t1, t2);

            double days = (t2 - t1).TotalDays;
            int a = 0;
        }
    }
}