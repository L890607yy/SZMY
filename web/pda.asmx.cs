﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;

namespace web
{
    /// <summary>
    /// pda 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://sanze.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class pda : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        string NTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        string NDtae = DateTime.Now.ToString("yyyy-MM-dd");
        string DNUM = DateTime.Now.ToString("yyyyMMdd");

        public decimal ToDecimal(object c)
        {
            return Falcon.Function.ToDecimal(c);
        }
        public decimal ToDecimal(object c, int weishu)
        {
            return ToDecimal(Decimal.Round(Decimal.Parse(Falcon.Function.ToDecimal(c) + ""), weishu) + "");
        }
        public int ToInt(object str)
        {
            return Falcon.Function.ToInt(str, 0);
        }

        [WebMethod]
        public string Tongbu(string jzx, string pct)
        {
            string save_jzx = "D:/_wwwroot/szmy/jzx.txt";
            string save_pct = "D:/_wwwroot/szmy/pct.txt";
            if (System.IO.File.Exists(save_jzx))
            {
                using (StreamWriter sw = new StreamWriter(save_jzx, false, Encoding.Default))
                {
                    sw.WriteLine(jzx);
                }
            }
            if (System.IO.File.Exists(save_pct))
            {
                using (StreamWriter sw = new StreamWriter(save_pct, false, Encoding.Default))
                {
                    sw.WriteLine(pct);
                }
            }
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            //List<Model.U_JZX> ljzx = (List<Model.U_JZX>)serializer.Deserialize(jzx/*要解析的字符*/, typeof(List<Model.U_JZX>/*实体*/));
            //List<Model.U_product> lpct = (List<Model.U_product>)serializer.Deserialize(jzx/*要解析的字符*/, typeof(List<Model.U_product>/*实体*/));

            List<Model.U_JZX> ljzx = jzx.Trim().Length == 0 ? (new List<Model.U_JZX>()) : (Tool.JsonHelper.JsonDeserialize<List<Model.U_JZX>>(jzx));
            List<Model.U_product> lpct = pct.Trim().Length == 0 ? (new List<Model.U_product>()) : (Tool.JsonHelper.JsonDeserialize<List<Model.U_product>>(pct));

            Database db = DatabaseFactory.CreateDatabase();
            bool inbz = true;

            using (DbConnection conn = db.CreateConnection())
            {
                conn.Open();
                DbTransaction tran = conn.BeginTransaction();
                foreach (Model.U_JZX one in ljzx)
                {
                    Model.U_JZX jzxm = new BLL.U_JZX().GetModelByCon(" xid='" + one.xid + "' ", tran);
                    if (jzxm != null && jzxm.id != 0) { }
                    else
                    {
                        int daynum = new BLL.U_JZX().GetMaxIDAtToday(NDtae, tran) + 1;
                        if (new BLL.U_JZX().Add(one.name, one.rongliang, 0, NDtae, one.addren, DateTime.Now, daynum, DNUM + daynum.ToString(), Config.defConfig.defDate, one.addren, one.isFin, one.xid, tran)) { } else { inbz = false; }
                    }
                }
                foreach (Model.U_product one in lpct)
                {
                    Model.U_JZX jzxm = new BLL.U_JZX().GetModelByCon(" xid='" + one.xid + "' ", tran);
                    Model.U_product pctm = new BLL.U_product().GetModelByCon(" xid='" + one.id + "'", tran);
                    int count = new BLL.U_product().GetCountBYjzxid(jzxm.id, tran) + 1;
                    if (pctm != null && pctm.id != 0)
                    {
                        if (new BLL.U_product().Update(pctm.id, pctm.jzxid, one.pbianma, one.pxinghao, one.pfanhao, one.hshui1, one.hshui2, one.addren, one.addtime, DateTime.Parse(one.addtime.ToString("yyyy/MM/dd")), one.erweima, one.pdaxid, one.pdaid, one.id, tran))
                        {
                            if (new BLL.U_JZX().Update(jzxm.id, jzxm.name, jzxm.rongliang, count, jzxm.riqi, jzxm.addren, jzxm.addtime, jzxm.daynum, jzxm.xiangbian, jzxm.chudate, jzxm.pdaid, jzxm.isFin, jzxm.xid, tran)) { } else { inbz = false; }
                        }
                        else { inbz = false; }
                    }
                    else
                    {
                        if (new BLL.U_product().Add(jzxm.id, one.pbianma, one.pxinghao, one.pfanhao, one.hshui1, one.hshui2, one.addren, one.addtime, DateTime.Parse(one.addtime.ToString("yyyy/MM/dd")), one.erweima, one.pdaxid, one.pdaid, one.id, tran))
                        {
                            if (new BLL.U_JZX().Update(jzxm.id, jzxm.name, jzxm.rongliang, count, jzxm.riqi, jzxm.addren, jzxm.addtime, jzxm.daynum, jzxm.xiangbian, jzxm.chudate, jzxm.pdaid, jzxm.isFin, jzxm.xid, tran)) { } else { inbz = false; }
                        }
                        else { inbz = false; }
                    }
                }
                if (inbz) { tran.Commit(); return "1"; } else { tran.Rollback(); return "0"; }
            }
        }
        [WebMethod]
        public string JZXtougbu(string where)
        {
            List<Model.U_JZX> ljzx = new BLL.U_JZX().GetListModel(where);

            if (ljzx != null && ljzx.Count != 0)
            {
                return Tool.JsonHelper.JsonSerializer<List<Model.U_JZX>>(ljzx);
            }
            else
            {
                return " ";
            }
        }

        [WebMethod]
        public string UserTongbu()
        {
            List<Model.Z_Users> luser = new BLL.Z_Users().GetListByWhere("");
            if (luser != null && luser.Count != 0)
            {
                foreach (Model.Z_Users one in luser)
                {
                    one.userpswd = Falcon.Function.Decrypt(one.userpswd);
                }
                return Tool.JsonHelper.JsonSerializer<List<Model.Z_Users>>(luser);
            }
            else
            {
                return " ";
            }
        }

        [WebMethod]
        public string UPpswd(int uid, string ymima, string xmima)
        {
            Model.Z_Users rym = new BLL.Z_Users().GetModel(uid);
            if (rym != null)
            {
                if (rym.userpswd == Falcon.Function.Encrypt(ymima.Trim()))
                {
                    if (new BLL.Z_Users().UpdatePswd(uid, ymima, xmima))
                    {
                        return "1";//修改成功；
                    }
                    else
                    {
                        return "-3";//修改不成功
                    }
                }
                else
                {
                    return "-2";//原密码不正确
                }
            }
            else
            {
                return "-1";//未查询到账户信息
            }
        }

        [WebMethod]
        public string Login(string uname, string pswd)
        {
            Model.Z_Users rym = new BLL.Z_Users().Login(uname);
            if (rym == null || rym.id == 0)
            {
                return "-1";
            }
            else
            {
                if (Falcon.Function.Encrypt(pswd.Trim()) == rym.userpswd)
                {
                    return Tool.JsonHelper.JsonSerializer<Model.Z_Users>(rym);
                }
                else
                {
                    return "-2";
                }
            }
        }


    }
}
