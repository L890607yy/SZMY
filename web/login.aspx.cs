﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web
{
    public partial class login : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AjaxPro.Utility.RegisterTypeForAjax(this.GetType());

                modelSettings = new BLL.Z_Settings().GetModel_Top();
            }
            modelSettings = modelSettings == null ? new Model.Z_Settings() { name = "精益软件管理系统", logo = "/images/jingyi_logo.png" } : modelSettings;
        }

        protected Model.Z_Settings modelSettings;

        [AjaxPro.AjaxMethod]
        public string Login(string username, string userpswd)
        {
            Model.Z_Users modelUsers = new BLL.Z_Users().Login(username);
            if (modelUsers == null || modelUsers.id == 0) return "当前账号不存在！";
            if (Falcon.Function.Encrypt(userpswd) != modelUsers.userpswd) return "登录失败！";
            if (modelUsers.isEnable == 0) return "当前账号已停用！";
            if (modelUsers.role == 0 ? false : new BLL.Z_Role().GetModel(modelUsers.role).isEnable == 0) return "所属角色已停用！";

            SessionUid = modelUsers.id;
            SessionRole = modelUsers.role;

            HttpCookie cookie = new HttpCookie("userinfo");
            cookie.Values["name"] = Server.UrlEncode(modelUsers.realname);
            //cookie.Expires = DateTime.MaxValue;
            cookie.Expires = System.DateTime.Now.AddDays(1);//设置过期时间  1天
            HttpContext.Current.Response.Cookies.Add(cookie);
            new BLL.Z_LoginLogs().Add(modelUsers.id, modelUsers.username, modelUsers.realname, Context.Request.UserHostAddress);//记录登录日志
            return "1";
        }

        [AjaxPro.AjaxMethod]
        public string LoginSuccess()
        {
            return "/admin/main.aspx";
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

        }
    }
}