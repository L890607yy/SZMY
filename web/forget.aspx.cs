﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web
{
    public partial class forget : Power
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            modelSettings = new BLL.Z_Settings().GetModel_Top();
            modelSettings = modelSettings == null ? new Model.Z_Settings() { name = "精益软件管理系统", logo = "/images/jingyi_logo.png" } : modelSettings;
        }
        protected Model.Z_Settings modelSettings;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string uname = txtUName.Text.Trim();
            if (uname.Length == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('请输入您的登录名！',{icon:5,time:1000});", true);
            }
            else
            {
                Model.Z_Users um = new BLL.Z_Users().Login(uname);
                if (um != null && um.id != 0)
                {
                    if (string.IsNullOrEmpty(um.email))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('系统检测到您尚未维护邮箱，请联系管理员重置密码！',{icon:5,time:5000});", true);
                    }
                    else
                    {
                        Model.Z_Settings sm = new BLL.Z_Settings().GetModel_Top();
                        if (sm.smtp.Trim().Length != 0 && sm.email.Trim().Length != 0 && sm.pswd.Trim().Length != 0)
                        {
                            Tool.MailHelper mail = new Tool.MailHelper(sm.smtp, sm.email, sm.pswd);
                            string mailbody = "<html><body><div> 您好，" + um.realname + "，您正在使用密码取回功能，原有系统密码为：" + Falcon.Function.Decrypt(um.userpswd) + "，如需修改，请登录系统后操作。请勿回复此邮件！</div></body></html>";
                            if (mail.SendMail(um.email, "密码", mailbody, null))
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('邮件已发送，请登录您在本系统的预留邮箱查看！',{icon:6,time:5000});", true);
                            }
                            else
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('邮件发送失败！',{icon:5,time:5000});", true);
                            }
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('系统检测到本系统未维护邮箱信息，请联系管理员重置密码！',{icon:5,time:5000});", true);
                        }
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "", "layer.msg('未查询到此用户信息！',{icon:5,time:5000});", true);
                }
            }
        }
    }
}