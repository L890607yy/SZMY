﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web.Ajax
{
    /// <summary>
    /// checkNameRepeat 的摘要说明
    /// </summary>
    public class checkNameRepeat : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string username = Falcon.Function.GetFormString("param");
            if (string.IsNullOrEmpty(username))
            {
                context.Response.Write("{\"info\":\"验证未通过，登录账号不能为空！\",\"status\":\"n\"}");
            }
            else
            {
                int isExist = new BLL.Z_Users().isExist_UserName(0, username);
                if (isExist == 0)
                    context.Response.Write("{\"info\":\"验证通过！\",\"status\":\"y\"}");
                else
                    context.Response.Write("{\"info\":\"验证未通过通过，已存在当前账号，请更换！\",\"status\":\"n\"}");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}