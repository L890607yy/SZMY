﻿using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace web.Ajax
{
    /// <summary>
    /// upload_json 的摘要说明
    /// </summary>
    public class upload_json : IHttpHandler
    {
        private HttpContext context;

        public void ProcessRequest(HttpContext context)
        {
            //type=1&type=2&size=1&rename=0
            //size单位为MB

            //String aspxUrl = context.Request.Path.Substring(0, context.Request.Path.LastIndexOf("/") + 1);

            //文件保存目录路径
            //String savePath = "../attached/";
            String savePath = "/uploadFile/";

            //文件保存目录URL
            //String saveUrl = aspxUrl + "../attached/";
            String saveUrl = "/uploadFile/";

            string type = Falcon.Function.GetQueryString("type");//0,3

            //0表示不限制上传文件扩展名
            Hashtable extTable = new Hashtable();
            if (string.IsNullOrEmpty(type) || type == "0")
            {
                //定义允许上传的文件扩展名
                extTable.Add("image", "gif,jpg,jpeg,png,bmp");//1
                extTable.Add("flash", "swf,flv");//2
                extTable.Add("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");//3
                extTable.Add("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");//4
            }
            else
            {
                string[] array_type = type.Split(',');
                if (array_type.Contains("1") || array_type.Contains("0")) extTable.Add("image", "gif,jpg,jpeg,png,bmp");//1
                if (array_type.Contains("2") || array_type.Contains("0")) extTable.Add("flash", "swf,flv");//2
                if (array_type.Contains("3") || array_type.Contains("0")) extTable.Add("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");//3
                if (array_type.Contains("4") || array_type.Contains("0")) extTable.Add("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");//4
            }

            //size:单位M
            decimal size = Falcon.Function.ToDecimal(Falcon.Function.GetQueryString("size"), 0);
            //最大文件大小，maxSize单位为字节B，默认最大1GB
            decimal maxSize = size == 0 ? 1073741824 : maxSize = size * 1048576;

            this.context = context;

            HttpPostedFile imgFile = context.Request.Files["imgFile"];
            if (imgFile == null)
            {
                showError("请选择文件。");
            }

            String dirPath = context.Server.MapPath(savePath);
            if (!Directory.Exists(dirPath))
            {
                //showError("上传目录不存在。");
                Directory.CreateDirectory(dirPath);
            }

            String dirName = context.Request.QueryString["dir"];
            if (String.IsNullOrEmpty(dirName))
            {
                dirName = "image";
            }
            if (!extTable.ContainsKey(dirName))
            {
                showError("目录名不正确。");
            }

            String fileName = imgFile.FileName;
            String fileExt = Path.GetExtension(fileName).ToLower();

            if (imgFile.InputStream == null || imgFile.InputStream.Length > maxSize)
            {
                showError("上传文件大小超过限制。");
            }

            if (String.IsNullOrEmpty(fileExt) || Array.IndexOf(((String)extTable[dirName]).Split(','), fileExt.Substring(1).ToLower()) == -1)
            {
                showError("上传文件扩展名是不允许的扩展名。\n只允许" + ((String)extTable[dirName]) + "格式。");
            }

            //创建文件夹
            dirPath += dirName + "/";
            saveUrl += dirName + "/";
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            String ymd = DateTime.Now.ToString("yyyyMMdd", DateTimeFormatInfo.InvariantInfo);
            dirPath += ymd + "/";
            saveUrl += ymd + "/";
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            String newFileName = Falcon.Function.GetQueryInt("rename", 1) == 0 ? fileName : (DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + fileExt);
            String filePath = dirPath + newFileName;

            imgFile.SaveAs(filePath);

            String fileUrl = saveUrl + newFileName;

            Hashtable hash = new Hashtable();
            hash["error"] = 0;
            hash["url"] = fileUrl;
            context.Response.AddHeader("Content-Type", "text/html; charset=UTF-8");
            context.Response.Write(JsonMapper.ToJson(hash));
            context.Response.End();
        }

        private void showError(string message)
        {
            Hashtable hash = new Hashtable();
            hash["error"] = 1;
            hash["message"] = message;
            context.Response.AddHeader("Content-Type", "text/html; charset=UTF-8");
            context.Response.Write(JsonMapper.ToJson(hash));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}