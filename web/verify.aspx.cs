﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace web
{
    public partial class verify : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // type:0-sesion,1-cookie 
            CreateCheckCodeImage(GenerateCheckCode(1));
        }

        private string GenerateCheckCode(int type)
        {
            int number;
            char code;
            string checkCode = String.Empty;

            System.Random random = new Random();

            for (int i = 0; i < 5; i++)
            {
                number = random.Next();

                if (number % 2 == 0)
                    code = (char)('0' + (char)(number % 10));
                else
                    code = (char)('A' + (char)(number % 26));

                checkCode += code.ToString();
            }

            if (type == 0)
            {
                System.Web.HttpContext.Current.Session["Verify"] = checkCode;
            }
            else
            {
                System.Web.HttpContext.Current.Response.Cookies.Add(new System.Web.HttpCookie("Verify", checkCode));
            }

            return checkCode;
        }

        private void CreateCheckCodeImage(string checkCode)
        {
            if (checkCode == null || checkCode.Trim() == String.Empty)
                return;

            System.Drawing.Bitmap image = new System.Drawing.Bitmap((int)Math.Ceiling((checkCode.Length * 18.0)), 32);
            Graphics g = Graphics.FromImage(image);

            try
            {
                //生成随机生成器
                Random random = new Random();

                //清空图片背景色
                g.Clear(Color.White);

                //画图片的背景噪音线
                for (int i = 0; i < 25; i++)
                {
                    int x1 = random.Next(image.Width);
                    int x2 = random.Next(image.Width);
                    int y1 = random.Next(image.Height);
                    int y2 = random.Next(image.Height);

                    g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
                }

                Font font = new System.Drawing.Font("Arial", 18, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic));
                System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Blue, Color.DarkRed, 1.2f, true);
                g.DrawString(checkCode, font, brush, 5, 3);

                //画图片的前景噪音点
                for (int i = 0; i < 100; i++)
                {
                    int x = random.Next(image.Width);
                    int y = random.Next(image.Height);

                    image.SetPixel(x, y, Color.FromArgb(random.Next()));
                }

                //画图片的边框线
                g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);

                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
                System.Web.HttpContext.Current.Response.ClearContent();
                System.Web.HttpContext.Current.Response.ContentType = "image/Gif";
                System.Web.HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            }
            finally
            {
                g.Dispose();
                image.Dispose();
            }
        }

        //public class VerifyCookieCode
        //{
        //    public VerifyCookieCode()
        //    {
        //        CreateCheckCodeImage(GenerateCheckCode());
        //    }

        //    private string GenerateCheckCode()
        //    {
        //        //创建整型型变量
        //        int number;
        //        //创建字符型变量
        //        char code;
        //        //创建字符串变量并初始化为空
        //        string checkCode = String.Empty;
        //        //创建Random对象
        //        Random random = new Random();
        //        //使用For循环生成4个数字
        //        for (int i = 0; i < 4; i++)
        //        {
        //            //生成一个随机数
        //            number = random.Next();
        //            //将数字转换成为字符型
        //            code = (char)('0' + (char)(number % 10));

        //            checkCode += code.ToString();
        //        }
        //        //将生成的随机数添加到Cookies中
        //        //System.Web.HttpContext.Current
        //        System.Web.HttpContext.Current.Response.Cookies.Add(new System.Web.HttpCookie("Verify", checkCode));
        //        //返回字符串
        //        return checkCode;
        //    }

        //    private void CreateCheckCodeImage(string checkCode)
        //    {
        //        //判断字符串不等于空和null
        //        if (checkCode == null || checkCode.Trim() == String.Empty)
        //            return;
        //        //创建一个位图
        //        System.Drawing.Bitmap image = new System.Drawing.Bitmap((int)Math.Ceiling((checkCode.Length * 18.0)), 32);
        //        Graphics g = Graphics.FromImage(image);

        //        try
        //        {
        //            //生成随机生成器
        //            Random random = new Random();

        //            //清空图片背景色
        //            g.Clear(Color.White);

        //            //画图片的背景噪音线
        //            for (int i = 0; i < 25; i++)
        //            {
        //                int x1 = random.Next(image.Width);
        //                int x2 = random.Next(image.Width);
        //                int y1 = random.Next(image.Height);
        //                int y2 = random.Next(image.Height);

        //                g.DrawLine(new Pen(Color.Silver), x1, y1, x2, y2);
        //            }

        //            Font font = new System.Drawing.Font("Arial", 18, (System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic));
        //            System.Drawing.Drawing2D.LinearGradientBrush brush = new System.Drawing.Drawing2D.LinearGradientBrush(new Rectangle(0, 0, image.Width, image.Height), Color.Blue, Color.DarkRed, 1.2f, true);
        //            g.DrawString(checkCode, font, brush, 5, 3);

        //            //画图片的前景噪音点
        //            for (int i = 0; i < 100; i++)
        //            {
        //                int x = random.Next(image.Width);
        //                int y = random.Next(image.Height);

        //                image.SetPixel(x, y, Color.FromArgb(random.Next()));
        //            }

        //            //画图片的边框线
        //            g.DrawRectangle(new Pen(Color.Silver), 0, 0, image.Width - 1, image.Height - 1);

        //            System.IO.MemoryStream ms = new System.IO.MemoryStream();
        //            image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
        //            System.Web.HttpContext.Current.Response.ClearContent();
        //            System.Web.HttpContext.Current.Response.ContentType = "image/Gif";
        //            System.Web.HttpContext.Current.Response.BinaryWrite(ms.ToArray());
        //        }
        //        finally
        //        {
        //            g.Dispose();
        //            image.Dispose();
        //        }
        //    }
        //}
    }
}