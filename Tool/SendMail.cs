﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Tool
{
    public class MailHelper
    {
        private string _smtpserver = "smtp.163.com";
        private string _email;
        private string _emailpswd;

        public MailHelper(string smtpserver, string email, string emailpswd)
        {
            _smtpserver = smtpserver;
            _email = email;
            _emailpswd = emailpswd;
        }

        public bool SendMail(string mailTo, string mailSubject, string mailBody, List<string> mailAttachs)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(_email, mailTo, mailSubject, mailBody);
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true;

            if (mailAttachs != null && mailAttachs.Count != 0)
            {
                foreach (string one in mailAttachs)
                {
                    if (System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath(one)))
                    {
                        System.Net.Mail.Attachment attachMent = new System.Net.Mail.Attachment(one, System.Net.Mime.MediaTypeNames.Application.Octet);
                        System.Net.Mime.ContentDisposition disposion = new System.Net.Mime.ContentDisposition();
                        disposion.CreationDate = System.IO.File.GetCreationTime(one);
                        disposion.ModificationDate = System.IO.File.GetLastWriteTime(one);
                        disposion.ReadDate = System.IO.File.GetLastAccessTime(one);
                        mail.Attachments.Add(attachMent);
                    }
                }
            }

            try
            {
                SmtpClient client = new SmtpClient(_smtpserver);
                client.UseDefaultCredentials = true;
                client.Credentials = new System.Net.NetworkCredential(_email, _emailpswd);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Send(mail);
                return true;
            }
            catch
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }
    }
}
