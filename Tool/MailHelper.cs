﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tool
{
    public class MailHelper
    {
        #region 属性

        private string _smtpserver = "smtp.163.com";
        private string _email;
        private string _emailpassword;
        private int _port = 25;

        public string SmtpServer
        {
            get { return _smtpserver; }
            set { _smtpserver = value; }
        }
        public string Mail
        {
            get { return _email; }
            set { _email = value; }
        }
        public string MailPswd
        {
            get { return _emailpassword; }
            set { _emailpassword = value; }
        }
        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }
        #endregion

        /// <summary>
        /// 发送邮件,可以附加附件
        /// </summary>
        /// <param name="mailTo">收件人地址</param>
        /// <param name="toDisplayName">收件人名称</param>
        /// <param name="mailFrom">发件人地址</param>
        /// <param name="fromDisplayName">发件人名称</param>
        /// <param name="mailBody">邮件内容</param>
        /// <param name="mailSubject">邮件主题</param>
        /// <param name="mailAttachs">附件路径</param>
        /// <returns>返回是否发送成功</returns>
        public bool SendMail(out string errs,string mailTo, string toDisplayName, string mailFrom, string fromDisplayName, string mailBody, string mailSubject, params string[] mailAttachs)
        {
            System.Net.Mail.MailAddress fromEmail = new System.Net.Mail.MailAddress(mailFrom, fromDisplayName);//发件人的地址和发件人名称
            System.Net.Mail.MailAddress toEmail = new System.Net.Mail.MailAddress(mailTo, toDisplayName);//收件人地址和收件人名称
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(fromEmail, toEmail);
            mail.Body = mailBody;
            mail.Subject = mailSubject;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = Encoding.UTF8;
            mail.Priority = System.Net.Mail.MailPriority.Normal;
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
            smtpClient.Host = _smtpserver;//这里设置成你的smtp主机名或者ip地址
            smtpClient.Port = _port;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtpClient.Credentials = new System.Net.NetworkCredential(_email, _emailpassword);//你的用户名和密码

            //以下是邮件附件部分,path一般是从FileUpload控件中取
            System.IO.FileInfo fi;
            if (mailAttachs != null && mailAttachs.Length != 0)
            {
                foreach (string one in mailAttachs)
                {
                    fi = new System.IO.FileInfo(one);
                    if (fi.Exists)
                    {
                        System.Net.Mail.Attachment attachMent = new System.Net.Mail.Attachment(one, System.Net.Mime.MediaTypeNames.Application.Octet);
                        System.Net.Mime.ContentDisposition disposion = new System.Net.Mime.ContentDisposition();
                        disposion.CreationDate = System.IO.File.GetCreationTime(one);
                        disposion.ModificationDate = System.IO.File.GetLastWriteTime(one);
                        disposion.ReadDate = System.IO.File.GetLastAccessTime(one);
                        mail.Attachments.Add(attachMent);
                    }
                }
            }
            try
            {
                smtpClient.Send(mail);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                errs = e.Message;
                return false;
            }
            errs = "";
            return true;
        }


        #region 发送多人，附件以参数形式传递

        public List<System.Net.Mail.Attachment> MailAttachment(List<string> listPath)
        {
            if (listPath != null && listPath.Count != 0)
            {
                List<System.Net.Mail.Attachment> list = list = new List<System.Net.Mail.Attachment>();
                foreach (string one in listPath)
                {
                    System.Net.Mail.Attachment attachMent = new System.Net.Mail.Attachment(one, System.Net.Mime.MediaTypeNames.Application.Octet);
                    System.Net.Mime.ContentDisposition disposion = new System.Net.Mime.ContentDisposition();
                    disposion.CreationDate = System.IO.File.GetCreationTime(one);
                    disposion.ModificationDate = System.IO.File.GetLastWriteTime(one);
                    disposion.ReadDate = System.IO.File.GetLastAccessTime(one);

                    list.Add(attachMent);
                }
                return list;
            }
            return null;
        }

        public bool SendMail(string mailTo, string toDisplayName, string mailFrom, string fromDisplayName, string mailBody, string mailSubject, List<System.Net.Mail.Attachment> listAttachement)
        {
            System.Net.Mail.MailAddress fromEmail = new System.Net.Mail.MailAddress(mailFrom, fromDisplayName);//发件人的地址和发件人名称
            System.Net.Mail.MailAddress toEmail = new System.Net.Mail.MailAddress(mailTo, toDisplayName);//收件人地址和收件人名称
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(fromEmail, toEmail);
            mail.Body = mailBody;
            mail.Subject = mailSubject;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = Encoding.UTF8;
            mail.Priority = System.Net.Mail.MailPriority.Normal;
            //mail.Attachments.Add(listAttachement);
            listAttachement.ForEach(m => mail.Attachments.Add(m));
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
            smtpClient.Host = _smtpserver;//这里设置成你的smtp主机名或者ip地址
            smtpClient.Port = _port;
            smtpClient.UseDefaultCredentials = false;
            smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtpClient.Credentials = new System.Net.NetworkCredential(_email, _emailpassword);//你的用户名和密码

            if (listAttachement != null && listAttachement.Count != 0)
            {
                foreach (System.Net.Mail.Attachment one in listAttachement)
                {
                    mail.Attachments.Add(one);
                }
            }
            try
            {
                smtpClient.Send(mail);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        #endregion
    }
}
