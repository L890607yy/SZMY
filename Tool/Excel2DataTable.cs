﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using org.in2bits.MyXls;
using System.Data.OleDb;
using System.IO;

namespace Tool
{
    /// <summary>
    /// Excel转DataTable
    /// </summary>
    public class Excel2DataTable
    {
        /// <summary>
        /// 生成DataTable
        /// </summary>
        /// <param name="excelPath">Excel</param>
        /// <param name="ex"></param>
        /// <returns></returns>
        //public static DataTable ExcelToDataTable(string excelPath, out string ex)  //把Excel里的数据转换为DataTable，并返回DataTable  
        //{
        //    //string strCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + System.Web.HttpContext.Current.Server.MapPath(excelPath) + ";Extended Properties='Excel 8.0;IMEX=1'";
        //    string strCon = "Provider=Microsoft.Ace.OleDb.12.0;" + "data source=" + System.Web.HttpContext.Current.Server.MapPath(excelPath) + ";Extended Properties='Excel 12.0; HDR=Yes;IMEX=1'";
        //    System.Data.OleDb.OleDbConnection Conn = new System.Data.OleDb.OleDbConnection(strCon);
        //    string strCom = "SELECT * FROM [Sheet1$]";
        //    DataTable dt;
        //    ex = string.Empty;

        //    try
        //    {
        //        Conn.Open();
        //        System.Data.OleDb.OleDbDataAdapter myCommand = new System.Data.OleDb.OleDbDataAdapter(strCom, Conn);
        //        DataSet ds = new DataSet();
        //        myCommand.Fill(ds, "[User Information$]");
        //        Conn.Close();
        //        dt = ds.Tables[0];
        //    }
        //    catch (Exception exception)
        //    {
        //        ex = exception.Message;
        //        return null;
        //    }
        //    return dt;
        //}

        public static DataTable ExcelToDataTable(string excelPath, string sheetName, bool isIncludeTitle, out string exception)
        {
            try
            {
                exception = string.Empty;
                string conStr = GetConStr(System.Web.HttpContext.Current.Server.MapPath(excelPath), isIncludeTitle);
                if (string.IsNullOrEmpty(conStr)) return null;
                OleDbConnection connection = new OleDbConnection(conStr);
                connection.Open();
                //string SheetName = "AP考核时段明细报表$";// = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                //if (string.IsNullOrEmpty(SheetName)) SheetName = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                //else if (!SheetName.Contains("$")) SheetName = SheetName + "$";
                if (string.IsNullOrEmpty(sheetName))
                {
                    sheetName = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                }
                if (!sheetName.Contains("$")) sheetName = sheetName + "$";

                OleDbDataAdapter adapter = new OleDbDataAdapter("select * from [" + sheetName + "]", conStr);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet, "[" + sheetName + "$]");
                connection.Close();
                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                exception = exception = "Excel打开失败:" + ex.Message.ToString().Replace(@"\", "");
                return null;
            }
        }

        public static string CheckExcel(string filePath, string[] ExcelColTitles, string TNAME, string sheetName)
        {
            string exceptions;
            string dterrs = "";
            try
            {
                DataTable dt = Tool.Excel2DataTable.ExcelToDataTable(filePath, sheetName, true, out exceptions);
                if (exceptions.Trim().Length == 0)
                {
                    int etcount = ExcelColTitles.Length;
                    for (int i = 0; i < etcount; i++)
                    {
                        dterrs += CheckDTCName(dt, ExcelColTitles[i]);
                    }
                    if (dterrs.Length != 0)
                    {
                        dterrs = "Excel 【" + TNAME + "】有以下错误：" + dterrs.Replace("$","");
                        //dterrs += finished(filePath, dterrs, TNAME);
                    }
                }
                else
                {
                    dterrs += ("Excel 【" + TNAME + "】：" + exceptions);
                    //dterrs += finished(filePath, exceptions, TNAME);
                }
            }
            catch { dterrs = "TRY包围"; }
            return dterrs;
        }
        //检查datatable中是否有指定列名
        public static string CheckDTCName(DataTable dt, string cname)
        {
            if (dt.Columns.Contains(cname))
            {
                return "";
            }
            else
            {
                return "Excel不存在名为【" + cname + "】的列！<br/>";
            }
        }

        public static bool ISEXCEL(string fileExtension)
        {
            bool fileOK = false;
            string[] allowExtension = new string[2];
            allowExtension[0] = ".xlsx";
            //allowExtension[1] = ".xlsx";
            //对上传的文件的类型进行判断
            for (int i = 0; i < 1; i++)
            {
                if (fileExtension == allowExtension[i])
                {
                    fileOK = true;
                }
            }
            return fileOK;
        }
        private static string GetConStr(string excelPath, bool isIncludeTitle)
        {
            string path = excelPath;
            if (!File.Exists(path)) return null;
            string extension = Path.GetExtension(path).ToLower();
            string HDR = string.Empty;//如果第一行是数据而不是标题的话, 应该写: "HDR=No;"
            if (isIncludeTitle)
            {
                HDR = "YES";//第一行是标题
            }
            else
            {
                HDR = "NO";//第一行是数据
            }
            if (extension == ".xls") return "Provider = Microsoft.Jet.OLEDB.4.0; Data Source =" + path + "; Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1;\"";
            if (extension == ".xlsx") return "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + path + "; Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1;\"";
            return null;
        }

        //public DataTable ExcelToDataTable(string ExcelPath)
        //{
        //    return ExcelToDataTable(ExcelPath, null);
        //}

        public static DataTable ExcelToDataTable(string excelPath, bool isIncludeTitle, out string exception)
        {
            try
            {
                exception = string.Empty;
                string conStr = GetConStr(System.Web.HttpContext.Current.Server.MapPath(excelPath), isIncludeTitle);
                if (string.IsNullOrEmpty(conStr)) return null;
                OleDbConnection connection = new OleDbConnection(conStr);
                connection.Open();
                string SheetName = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                //if (string.IsNullOrEmpty(SheetName)) SheetName = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                //else if (!SheetName.Contains("$")) SheetName = SheetName + "$";

                OleDbDataAdapter adapter = new OleDbDataAdapter("select * from [" + SheetName + "]", conStr);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet, "[" + SheetName + "$]");
                connection.Close();
                return dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                exception = exception = "Excel打开失败:<br/>" + ex.Message.ToString().Replace(@"\", "");
                return null;
            }
        }
    }

    /// <summary>
    /// DataTable转Excel
    /// </summary>
    public class DataTable2Excel
    {
        /// <summary>
        /// DataTable生成Excel，需引用org.in2bits.MyXls.dll
        /// </summary>
        /// <param name="Header">大标题，为null表示没有大标题</param>
        /// <param name="fileName">Excel文件名，直接传汉字即可</param>
        /// <param name="dtSource">数据源</param>
        /// <param name="paraColumnHeader">new string[] { "用户名,username", "登录IP,ip", "登录时间,logintime" })，前面是列明，后面是数据库字段</param>
        public static void CreateExcel(string Header, string fileName, System.Data.DataTable dtSource, params  string[] paraColumnHeader)
        {
            if (dtSource != null && dtSource.Rows.Count != 0)
            {
                XlsDocument xls = new XlsDocument();
                fileName = System.Web.HttpUtility.UrlEncode(fileName);
                setXlsColumnHeader(Header, xls, dtSource, paraColumnHeader);

                //保存
                xls.FileName = fileName;
                //保存文档
                //xls.Save(folder);//保存到服务器
                xls.Send();
            }
        }
        /// <summary>
        /// 将datatable中的数据保存到csv中
        /// </summary>
        /// <param name="dt">数据来源</param>
        /// <param name="savaPath">保存的路径</param>
        /// <param name="strName">保存文件的名称</param>
        public static void ExportToSvc(System.Data.DataTable dt, string savaPath, string strName)
        {
            string strPath = Path.GetTempPath() + strName + ".csv";//保存到本项目文件夹下

            //string strPath = savaPath + "\\" + strName + ".csv";//保存到指定目录下

            if (File.Exists(strPath))
            {
                File.Delete(strPath);
            }
            //先打印标头
            StringBuilder strColu = new StringBuilder();
            StringBuilder strValue = new StringBuilder();
            int i = 0;
            try
            {
                StreamWriter sw = new StreamWriter(new FileStream(strPath, FileMode.CreateNew), Encoding.GetEncoding("GB2312"));
                for (i = 0; i <= dt.Columns.Count - 1; i++)
                {
                    strColu.Append(dt.Columns[i].ColumnName);
                    strColu.Append(",");
                }
                strColu.Remove(strColu.Length - 1, 1);//移出掉最后一个,字符
                sw.WriteLine(strColu);
                foreach (DataRow dr in dt.Rows)
                {
                    strValue.Remove(0, strValue.Length);//移出
                    for (i = 0; i <= dt.Columns.Count - 1; i++)
                    {
                        strValue.Append(dr[i].ToString());
                        strValue.Append(",");
                    }
                    strValue.Remove(strValue.Length - 1, 1);//移出掉最后一个,字符
                    sw.WriteLine(strValue);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
            }
            System.Diagnostics.Process.Start(strPath);
        }
        private static void setXlsColumnHeader(string Header, XlsDocument xls, System.Data.DataTable dt, params string[] paraColumnHeader)
        {
            Worksheet sheet = xls.Workbook.Worksheets.Add("Sheet1");

            //设置文档列属性 
            ColumnInfo cinfo = new ColumnInfo(xls, sheet);//设置xls文档的指定工作页的列属性
            cinfo.Collapsed = true;
            //设置列的范围 如 0列-10列
            cinfo.ColumnIndexStart = 0;//列开始
            cinfo.ColumnIndexEnd = 10;//列结束
            cinfo.Collapsed = true;
            cinfo.Width = 90 * 60;//列宽度
            sheet.AddColumnInfo(cinfo);
            //设置文档列属性结束

            //创建列
            Cells cells = sheet.Cells; //获得指定工作页列集合 
            int rowIndex = 1;//判断是否存在大标题
            if (!string.IsNullOrEmpty(Header))//如果有大标题
            {
                rowIndex = 2;
                //创建列样式创建列时引用
                XF cellXF = xls.NewXF();
                cellXF.VerticalAlignment = VerticalAlignments.Centered;
                cellXF.HorizontalAlignment = HorizontalAlignments.Centered;
                cellXF.Font.Height = 18 * 12;
                cellXF.Font.Bold = true;
                cellXF.Pattern = 0;//设定单元格填充风格。如果设定为0，则是纯色填充
                //cellXF.PatternBackgroundColor = Colors.Red;//填充的背景底色
                //cellXF.PatternColor = Colors.Red;//设定填充线条的颜色
                //创建列样式结束 

                //列操作基本
                cells.Add(1, 1, Header, cellXF);//添加标题列返回一个列  参数：行 列 名称 样式对象     
                sheet.AddMergeArea(new MergeArea(1, 1, 1, paraColumnHeader.Length));
            }



            //创建子列表头，定义模板
            XF cellXFSubtit = xls.NewXF();
            cellXFSubtit.VerticalAlignment = VerticalAlignments.Centered;
            cellXFSubtit.HorizontalAlignment = HorizontalAlignments.Centered;
            cellXFSubtit.Font.Height = 15 * 12;
            cellXFSubtit.Font.Bold = true;
            cellXFSubtit.Pattern = 0;
            for (int i = 0; i < paraColumnHeader.Length; i++)
            {
                cells.Add(rowIndex, i + 1, paraColumnHeader[i].Split(',')[0], cellXFSubtit);
            }

            //创建数据 
            if (dt != null && dt.Rows.Count != 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    for (int j = 0; j < paraColumnHeader.Length; j++)
                    {
                        cells.Add(i + rowIndex + 1, j + 1, dt.Rows[i][paraColumnHeader[j].Split(',')[1]].ToString());
                    }
                }
            }
        }
    }
}
