﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

namespace Tool
{
    public static class Pager
    {
        /// <summary>
        /// 分页查询，返回DataSet，调用存储过程Query
        /// </summary>
        /// <param name="PageIndex">当前页</param>
        /// <param name="PageSize">每页输出的记录数</param>
        /// <param name="tableName">要查询的对象，表名或试图名</param>
        /// <param name="fields">要查询的字段，用逗号分隔，全部显示用"*"</param>
        /// <param name="filter">要查询的条件，不包含where，如"name='张三'"</param>
        /// <param name="order">排序，不包含order by，如"sort asc,id desc"</param>
        /// <param name="sort">排序级别，1：按主键升序，2：按主键降序，3：混合排序（混合排序最后必须包含主键，否则会出错！）</param>
        /// <returns></returns>
        public static DataSet Query(int PageIndex, int PageSize, string tableName, string fields, string filter, string order, int sort)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetStoredProcCommand("Query");
            db.AddInParameter(cmd, "@TableName", DbType.String, tableName);
            db.AddInParameter(cmd, "@FieldList", DbType.String, fields);
            db.AddInParameter(cmd, "@PrimaryKey", DbType.String, "id");
            db.AddInParameter(cmd, "@Where", DbType.String, filter);
            db.AddInParameter(cmd, "@Order", DbType.String, order);
            db.AddInParameter(cmd, "@SortType", DbType.Int32, sort);
            db.AddInParameter(cmd, "@PageIndex", DbType.Int32, PageIndex);
            db.AddInParameter(cmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(cmd, "@RecorderCount", DbType.Int32, 0);
            db.AddParameter(cmd, "@TotalCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);
            db.AddParameter(cmd, "@TotalPageCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);

            DataSet ds = db.ExecuteDataSet(cmd);

            return ds;
        }

        /// <summary>
        /// 分页查询，返回DataSet，调用存储过程Query
        /// </summary>
        /// <param name="PageIndex">当前页</param>
        /// <param name="PageSize">每页输出的记录数</param>
        /// <param name="totalCount">返回查询结果的总记录数</param>
        /// <param name="pageCount">返回查询结果的总页数</param>
        /// <param name="tableName">要查询的对象，表名或试图名</param>
        /// <param name="fields">要查询的字段，用逗号分隔，全部显示用"*"</param>
        /// <param name="filter">要查询的条件，不包含where，如"name='张三'"</param>
        /// <param name="order">排序，不包含order by，如"sort asc,id desc"</param>
        /// <param name="sort">排序级别，1：按主键升序，2：按主键降序，3：混合排序（混合排序最后必须包含主键，否则会出错！）</param>
        /// <returns></returns>
        public static DataSet Query(int PageIndex, int PageSize, out int totalCount, out int pageCount, string tableName, string fields, string filter, string order, int sort)
        {
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetStoredProcCommand("Query");
            db.AddInParameter(cmd, "@TableName", DbType.String, tableName);
            db.AddInParameter(cmd, "@FieldList", DbType.String, fields);
            db.AddInParameter(cmd, "@PrimaryKey", DbType.String, "id");
            db.AddInParameter(cmd, "@Where", DbType.String, filter);
            db.AddInParameter(cmd, "@Order", DbType.String, order);
            db.AddInParameter(cmd, "@SortType", DbType.Int32, sort);
            db.AddInParameter(cmd, "@PageIndex", DbType.Int32, PageIndex);
            db.AddInParameter(cmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(cmd, "@RecorderCount", DbType.Int32, 0);

            db.AddParameter(cmd, "@TotalCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);
            db.AddParameter(cmd, "@TotalPageCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);

            DataSet ds = db.ExecuteDataSet(cmd);
            totalCount = Convert.ToInt32(db.GetParameterValue(cmd, "TotalCount"));
            pageCount = Convert.ToInt32(db.GetParameterValue(cmd, "TotalPageCount"));

            return ds;
        }


        /// <summary>
        /// 分页查询，返回DataSet，调用存储过程Query
        /// </summary>
        /// <param name="conn">web.config中连接字符串的name</param>
        /// <param name="PageIndex">当前页</param>
        /// <param name="PageSize">每页输出的记录数</param>
        /// <param name="totalCount">返回查询结果的总记录数</param>
        /// <param name="pageCount">返回查询结果的总页数</param>
        /// <param name="tableName">要查询的对象，表名或试图名</param>
        /// <param name="fields">要查询的字段，用逗号分隔，全部显示用"*"</param>
        /// <param name="filter">要查询的条件，不包含where，如"name='张三'"</param>
        /// <param name="order">排序，不包含order by，如"sort asc,id desc"</param>
        /// <param name="sort">排序级别，1：按主键升序，2：按主键降序，3：混合排序（混合排序最后必须包含主键，否则会出错！）</param>
        /// <returns></returns>
        public static DataSet Query(string conn, int PageIndex, int PageSize, out int totalCount, out int pageCount, string tableName, string fields, string filter, string order, int sort)
        {
            Database db = DatabaseFactory.CreateDatabase(conn);
            DbCommand cmd = db.GetStoredProcCommand("Query");
            db.AddInParameter(cmd, "@TableName", DbType.String, tableName);
            db.AddInParameter(cmd, "@FieldList", DbType.String, fields);
            db.AddInParameter(cmd, "@PrimaryKey", DbType.String, "id");
            db.AddInParameter(cmd, "@Where", DbType.String, filter);
            db.AddInParameter(cmd, "@Order", DbType.String, order);
            db.AddInParameter(cmd, "@SortType", DbType.Int32, sort);
            db.AddInParameter(cmd, "@PageIndex", DbType.Int32, PageIndex);
            db.AddInParameter(cmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(cmd, "@RecorderCount", DbType.Int32, 0);

            db.AddParameter(cmd, "@TotalCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);
            db.AddParameter(cmd, "@TotalPageCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);

            DataSet ds = db.ExecuteDataSet(cmd);
            totalCount = Convert.ToInt32(db.GetParameterValue(cmd, "TotalCount"));
            pageCount = Convert.ToInt32(db.GetParameterValue(cmd, "TotalPageCount"));

            return ds;
        }

        /// <summary>
        /// 分页查询，返回List，调用存储过程Query
        /// </summary>
        /// <typeparam name="T">返回的对象</typeparam> 
        /// <param name="PageIndex">当前页</param>
        /// <param name="PageSize">每页输出的记录数</param>
        /// <param name="tableName">要查询的对象，表名或试图名</param>
        /// <param name="fields">要查询的字段，用逗号分隔，全部显示用"*"</param>
        /// <param name="filter">要查询的条件，不包含where，如"name='张三'"</param>
        /// <param name="order">排序，不包含order by，如"sort asc,id desc"</param>
        /// <param name="sort">排序级别，1：按主键升序，2：按主键降序，3：混合排序（混合排序最后必须包含主键，否则会出错！）</param>
        /// <returns></returns>
        public static List<T> Query<T>(int PageIndex, int PageSize, string tableName, string fields, string filter, string order, int sort)
        {
            List<T> list = new List<T>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetStoredProcCommand("Query");
            db.AddInParameter(cmd, "@TableName", DbType.String, tableName);
            db.AddInParameter(cmd, "@FieldList", DbType.String, fields);
            db.AddInParameter(cmd, "@PrimaryKey", DbType.String, "id");
            db.AddInParameter(cmd, "@Where", DbType.String, filter);
            db.AddInParameter(cmd, "@Order", DbType.String, order);
            db.AddInParameter(cmd, "@SortType", DbType.Int32, sort);
            db.AddInParameter(cmd, "@PageIndex", DbType.Int32, PageIndex);
            db.AddInParameter(cmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(cmd, "@RecorderCount", DbType.Int32, 0);

            db.AddParameter(cmd, "@TotalCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);
            db.AddParameter(cmd, "@TotalPageCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);

            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<T>(reader);
            }

            return list;
        }

        /// <summary>
        /// 分页查询，返回List，调用存储过程Query
        /// </summary>
        /// <typeparam name="T">返回的对象</typeparam>
        /// <param name="PageIndex">当前页</param>
        /// <param name="PageSize">每页输出的记录数</param>
        /// <param name="totalCount">返回查询结果的总记录数</param>
        /// <param name="pageCount">返回查询结果的总页数</param>
        /// <param name="tableName">要查询的对象，表名或试图名</param>
        /// <param name="fields">要查询的字段，用逗号分隔，全部显示用"*"</param>
        /// <param name="filter">要查询的条件，不包含where，如"name='张三'"</param>
        /// <param name="order">排序，不包含order by，如"sort asc,id desc"</param>
        /// <param name="sort">排序级别，1：按主键升序，2：按主键降序，3：混合排序（混合排序最后必须包含主键，否则会出错！）</param>
        /// <returns></returns>
        public static List<T> Query<T>(int PageIndex, int PageSize, out int totalCount, out int pageCount, string tableName, string fields, string filter, string order, int sort)
        {
            List<T> list = new List<T>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetStoredProcCommand("Query");
            db.AddInParameter(cmd, "@TableName", DbType.String, tableName);
            db.AddInParameter(cmd, "@FieldList", DbType.String, fields);
            db.AddInParameter(cmd, "@PrimaryKey", DbType.String, "id");
            db.AddInParameter(cmd, "@Where", DbType.String, filter);
            db.AddInParameter(cmd, "@Order", DbType.String, order);
            db.AddInParameter(cmd, "@SortType", DbType.Int32, sort);
            db.AddInParameter(cmd, "@PageIndex", DbType.Int32, PageIndex);
            db.AddInParameter(cmd, "@PageSize", DbType.Int32, PageSize);
            db.AddInParameter(cmd, "@RecorderCount", DbType.Int32, 0);

            db.AddParameter(cmd, "@TotalCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);
            db.AddParameter(cmd, "@TotalPageCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);

            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<T>(reader);
            }
            totalCount = Convert.ToInt32(db.GetParameterValue(cmd, "TotalCount"));
            pageCount = Convert.ToInt32(db.GetParameterValue(cmd, "TotalPageCount"));

            return list;
        }


        /// <summary>
        /// 分页查询，返回List，过滤重复数据，调用存储过程DistinctQuery
        /// </summary>
        /// <typeparam name="T"></typeparam> 
        /// <param name="PageIndex">当前页</param>
        /// <param name="PageSize">每页输出的记录数</param>
        /// <param name="totalCount">返回查询结果的总记录数</param>
        /// <param name="pageCount">返回查询结果的总页数</param>
        /// <param name="tableName">要查询的对象，表名或试图名</param>
        /// <param name="fields">要查询的字段，用逗号分隔，全部显示用"*"</param>
        /// <param name="filter">要查询的条件，不包含where，如"name='张三'"</param>
        /// <param name="order">排序，不包含order by，如"sort asc,id desc"</param>
        /// <param name="sort">排序级别，1：按主键升序，2：按主键降序，3：混合排序（混合排序最后必须包含主键，否则会出错！）</param>
        /// <returns></returns>
        public static List<T> DistinctQuery<T>(int PageIndex, int PageSize, out int totalCount, out int pageCount, string tableName, string fields, string filter, string order, int sort)
        {
            List<T> list = new List<T>();
            Database db = DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetStoredProcCommand("DistinctQuery");

            db.AddInParameter(cmd, "@tableName", DbType.String, tableName);
            db.AddInParameter(cmd, "@fieldNames", DbType.String, fields);
            db.AddInParameter(cmd, "@pageSize", DbType.Int32, PageSize);
            db.AddInParameter(cmd, "@page", DbType.Int32, PageIndex);
            db.AddInParameter(cmd, "@fieldSort", DbType.String, order);
            db.AddInParameter(cmd, "@sort", DbType.Byte, sort);
            db.AddInParameter(cmd, "@condition", DbType.String, filter);
            db.AddInParameter(cmd, "@keyID", DbType.String, "id");
            db.AddInParameter(cmd, "@distinct", DbType.Int32, 1);

            db.AddParameter(cmd, "@counts", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);
            db.AddParameter(cmd, "@pageCount", DbType.Int32, ParameterDirection.Output, null, DataRowVersion.Current, null);


            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<T>(reader);
            }
            totalCount = Convert.ToInt32(db.GetParameterValue(cmd, "counts"));
            pageCount = Convert.ToInt32(db.GetParameterValue(cmd, "pageCount"));

            return list;
        }
    }
}
