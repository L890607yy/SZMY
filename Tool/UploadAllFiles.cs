﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web;

namespace Tool
{
    public static class UploadAllFiles
    {
        /// <summary>
        /// 图片上传
        /// </summary>
        /// <param name="url">上传成功后返回的图片路径</param>
        /// <param name="FileUpload1"></param>
        /// <param name="path">上传保存的站点路径</param>
        /// <param name="size">允许上传图片大小，单位为K</param>
        /// <param name="w1">图片缩放宽度</param>
        /// <param name="h1">图片缩放高度</param>
        /// <returns></returns>
        public static bool UploadImg(out string url, FileUpload FileUpload1, string path, int size, int w1, int h1)
        {
            //上传
            url = "上传失败";
            if (FileUpload1.HasFile)
            {
                string error = "";
                Falcon.UpLoad upload = new Falcon.UpLoad();
                upload.Path = path;
                Falcon.ImageOP op = Falcon.UpLoad.GetImageOP(FileUpload1.PostedFile, size, out error);
                if (op == null)
                {
                    url = error;
                    return false;
                }
                op.ZoomAuto(w1, h1);
                string src = "";
                if (upload.SaveAs(out src, op, false))
                {
                    url = src;
                    return true;
                }
                url = src;
                return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 图片上传
        /// </summary>
        /// <param name="url">上传成功后返回的图片路径</param>
        /// <param name="FileUpload1"></param>
        /// <param name="path">上传保存的站点路径</param>
        /// <param name="size">允许上传图片大小，单位为K</param>
        /// <param name="w1">图片缩放宽度</param>
        /// <param name="h1">图片缩放高度</param>
        /// <param name="w2">图片缩略图缩放宽度</param>
        /// <param name="h2">图片缩略图缩放高度</param>
        /// <returns></returns>
        public static bool UploadImg(out string url, FileUpload FileUpload1, string path, int size, int w1, int h1, int w2, int h2)
        {
            //上传
            url = "上传失败";
            if (FileUpload1.HasFile)
            {
                string error = "";
                Falcon.UpLoad upload = new Falcon.UpLoad();
                upload.Path = path;
                Falcon.ImageOP op = Falcon.UpLoad.GetImageOP(FileUpload1.PostedFile, size, out error);
                if (op == null)
                {
                    url = error;
                    return false;
                }
                op.ZoomAuto(w1, h1);
                string src = "";
                if (upload.SaveAs(out src, op, false))
                {
                    op.ZoomAuto(w2, h2);
                    string smallpic = src.Replace(".jpg", "_m.jpg");
                    if (op.Save(System.Web.HttpContext.Current.Server.MapPath("~" + smallpic)))
                    {
                        op.Dispose();

                        //if (System.Configuration.ConfigurationSettings.AppSettings["FullURL"] == "1")
                        //{
                        //    string root = Falcon.Tools.UrlPath.GetRootPath();
                        //    root = root.EndsWith("/") ? root.Substring(0, root.Length - 1) : root;
                        //    smallpic = root + smallpic;
                        //}
                        url = smallpic;
                        return true;
                    }
                    return false;
                }
                url = src;
                return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 图片上传
        /// </summary>
        /// <param name="url">上传成功后返回的图片路径</param>
        /// <param name="file"></param>
        /// <param name="path">上传保存的站点路径</param>
        /// <param name="size">允许上传图片大小，单位为K</param>
        /// <param name="w1">图片缩放宽度</param>
        /// <param name="h1">图片缩放高度</param>
        /// <returns></returns>
        public static bool UploadImg(out string url, HttpPostedFile file, string path, int size, int w1, int h1)
        {
            //上传
            url = "上传失败";
            if (file.ContentLength != 0)
            {
                string error = "";
                Falcon.UpLoad upload = new Falcon.UpLoad();
                upload.Path = path;
                Falcon.ImageOP op = Falcon.UpLoad.GetImageOP(file, size, out error);
                if (op == null)
                {
                    url = error;
                    return false;
                }
                op.ZoomAuto(w1, h1);
                string src = "";
                if (upload.SaveAs(out src, op, false))
                {
                    url = src;
                    return true;
                }
                url = src;
                return false;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 图片上传
        /// </summary>
        /// <param name="url">上传成功后返回的图片路径</param>
        /// <param name="file"></param>
        /// <param name="path">上传保存的站点路径</param>
        /// <param name="size">允许上传图片大小，单位为K</param>
        /// <param name="w1">图片缩放宽度</param>
        /// <param name="h1">图片缩放高度</param>
        /// <param name="w2">图片缩略图缩放宽度</param>
        /// <param name="h2">图片缩略图缩放高度</param>
        /// <returns></returns>
        public static bool UploadImg(out string url, HttpPostedFile file, string path, int size, int w1, int h1, int w2, int h2)
        {
            //上传
            url = "上传失败";
            if (file.ContentLength != 0)
            {
                string error = "";
                Falcon.UpLoad upload = new Falcon.UpLoad();
                upload.Path = path;
                Falcon.ImageOP op = Falcon.UpLoad.GetImageOP(file, size, out error);
                if (op == null)
                {
                    url = error;
                    return false;
                }
                op.ZoomAuto(w1, h1);
                string src = "";
                if (upload.SaveAs(out src, op, false))
                {
                    op.ZoomAuto(w2, h2);
                    string smallpic = src.Replace(".jpg", "_m.jpg");
                    if (op.Save(System.Web.HttpContext.Current.Server.MapPath("~" + smallpic)))
                    {
                        op.Dispose();

                        string root = Falcon.UrlPath.GetRootPath();
                        root = root.EndsWith("/") ? root.Substring(0, root.Length - 1) : root;
                        smallpic = root + smallpic;

                        url = smallpic;
                        return true;
                    }
                    return false;
                }
                url = src;
                return false;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="url">上传成功后返回的图片路径</param>
        /// <param name="FileUpload1"></param>
        /// <param name="FileType">文件类型"flv|rmvb|avi"</param>
        /// <param name="path">上传保存的站点路径</param>
        /// <param name="size">允许上传文件大小，单位为K</param>
        /// <returns></returns>
        public static bool UploadFile(out string url, FileUpload FileUpload1, string FileType, string path, int size)
        {
            url = "上传失败";
            if (FileUpload1.HasFile)
            {
                Falcon.UpLoad upload = new Falcon.UpLoad();
                upload.Path = path;
                upload.FileType = FileType;
                upload.Sizes = size;

                string src = "";

                if (upload.SaveAs(out src, FileUpload1))
                {
                    url = src;
                    return true;
                }
                url = src;
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
