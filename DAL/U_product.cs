﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common; 

namespace DAL{ 
	public class U_product
	{
		Database db;
		
		public U_product()
		{
		 	db = DatabaseFactory.CreateDatabase();
		}

        #region 增加

        public int Add(int jzxid, string pbianma, string pxinghao, string pfanhao, string hshui1, string hshui2, int addren, DateTime addtime, DateTime adddate, string erweima, int pdaxid, int pdaid, int xid, DbTransaction tran)
        {
            string sql = "insert into U_product(jzxid,pbianma,pxinghao,pfanhao,hshui1,hshui2,addren,addtime,adddate,erweima,pdaxid,pdaid,xid) values (@jzxid,@pbianma,@pxinghao,@pfanhao,@hshui1,@hshui2,@addren,@addtime,@adddate,@erweima,@pdaxid,@pdaid,@xid);select @@identity";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@jzxid", System.Data.DbType.Int32, jzxid);
            db.AddInParameter(cmd, "@pbianma", System.Data.DbType.String, pbianma);
            db.AddInParameter(cmd, "@pxinghao", System.Data.DbType.String, pxinghao);
            db.AddInParameter(cmd, "@pfanhao", System.Data.DbType.String, pfanhao);
            db.AddInParameter(cmd, "@hshui1", System.Data.DbType.String, hshui1);
            db.AddInParameter(cmd, "@hshui2", System.Data.DbType.String, hshui2);
            db.AddInParameter(cmd, "@addren", System.Data.DbType.Int32, addren);
            db.AddInParameter(cmd, "@addtime", System.Data.DbType.DateTime, addtime);
            db.AddInParameter(cmd, "@adddate", System.Data.DbType.DateTime, adddate);
            db.AddInParameter(cmd, "@erweima", System.Data.DbType.String, erweima);
            db.AddInParameter(cmd, "@pdaxid", System.Data.DbType.Int32, pdaxid);
            db.AddInParameter(cmd, "@pdaid", System.Data.DbType.Int32, pdaid);
            db.AddInParameter(cmd, "@xid", System.Data.DbType.Int32, xid);

            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 修改

        public int Update(int id, int jzxid, string pbianma, string pxinghao, string pfanhao, string hshui1, string hshui2, int addren, DateTime addtime, DateTime adddate, string erweima, int pdaxid, int pdaid, int xid, DbTransaction tran)
        {
            string sql = "update U_product set jzxid=@jzxid,pbianma=@pbianma,pxinghao=@pxinghao,pfanhao=@pfanhao,hshui1=@hshui1,hshui2=@hshui2,addren=@addren,addtime=@addtime,adddate=@adddate,erweima=@erweima,pdaxid=@pdaxid,pdaid=@pdaid,xid=@xid where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@jzxid", System.Data.DbType.Int32, jzxid);
            db.AddInParameter(cmd, "@pbianma", System.Data.DbType.String, pbianma);
            db.AddInParameter(cmd, "@pxinghao", System.Data.DbType.String, pxinghao);
            db.AddInParameter(cmd, "@pfanhao", System.Data.DbType.String, pfanhao);
            db.AddInParameter(cmd, "@hshui1", System.Data.DbType.String, hshui1);
            db.AddInParameter(cmd, "@hshui2", System.Data.DbType.String, hshui2);
            db.AddInParameter(cmd, "@addren", System.Data.DbType.Int32, addren);
            db.AddInParameter(cmd, "@addtime", System.Data.DbType.DateTime, addtime);
            db.AddInParameter(cmd, "@adddate", System.Data.DbType.DateTime, adddate);
            db.AddInParameter(cmd, "@erweima", System.Data.DbType.String, erweima);
            db.AddInParameter(cmd, "@pdaxid", System.Data.DbType.Int32, pdaxid);
            db.AddInParameter(cmd, "@pdaid", System.Data.DbType.Int32, pdaid);
            db.AddInParameter(cmd, "@xid", System.Data.DbType.Int32, xid);


            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }
        #endregion		
		 
		 
		#region 查询
        public int GetCountBYjzxid(int jzxid, DbTransaction tran)
        {
            if (jzxid != 0)
            {
                string sql = "select count(id) from U_product where jzxid='" + jzxid + "' ";
                DbCommand cmd = db.GetSqlStringCommand(sql);

                if(tran==null) 
                    return Falcon.Function.ToInt(db.ExecuteDataSet(cmd).Tables[0].Rows[0][0].ToString(),0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteDataSet(cmd,tran).Tables[0].Rows[0][0].ToString(), 0);
            }
            else { return 0; }
        }
		public Model.U_product GetModel(int id)
		{
		 	string sql="select * from U_product where id=@id";
			DbCommand cmd = db.GetSqlStringCommand(sql);
			            db.AddInParameter(cmd, "@id",System.Data.DbType.Int32, id);         
               
			Model.U_product model;
			using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.U_product>(reader);
            }
            return model;
		}
        public Model.U_product GetModelByCon(string where, DbTransaction tran)
		{
			if(string.IsNullOrEmpty(where)) { return null; }
			else{
		 		string sql="select * from U_product ";
				if(where.Length!=0) sql+=(" where "+where);
				DbCommand cmd = db.GetSqlStringCommand(sql);
                Model.U_product model;
                if (tran == null)
                {
                    using (var reader = db.ExecuteReader(cmd))
                    {
                        model = Tool.DataReaderToModel.ReaderToModel<Model.U_product>(reader);
                    }
                }
                else
                {
                    cmd.Transaction = tran;
                    cmd.Connection = tran.Connection;
                    using (var reader = db.ExecuteReader(cmd, tran))
                    {
                        model = Tool.DataReaderToModel.ReaderToModel<Model.U_product>(reader);
                    }
                }
				return model;
			}
		}	
		public List<Model.U_product> GetListModel(string where)
		{
		 	string sql="select * from U_product ";
			if(where.Length!=0) sql+=(" where "+where);
			DbCommand cmd = db.GetSqlStringCommand(sql);
			   
			List<Model.U_product> listmodel;
			using (var reader = db.ExecuteReader(cmd))
            {
                listmodel = Tool.DataReaderToModel.ReaderToListModel<Model.U_product>(reader);
            }
            return listmodel;
		}
		#endregion		
		
		#region  删除
		
		public int Delete(int id,DbTransaction tran)
		{
		 	string sql="delete from U_product where id=@id";
			DbCommand cmd = db.GetSqlStringCommand(sql);
			            db.AddInParameter(cmd, "@id",System.Data.DbType.Int32, id);         
               
            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            } 
		}
		#endregion
	}
}