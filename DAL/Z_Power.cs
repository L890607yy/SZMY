﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DAL
{
    public class Z_Forms
    {
        #region 添加

        public int Add(string name, string url, int pid, string power, int sort, int isMenu, int isEnable, string remark)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "insert into Z_Forms (name,url,pid,power,sort,isMenu,isEnable,remark) values (@name,@url,@pid,@power,@sort,@isMenu,@isEnable,@remark)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@url", System.Data.DbType.String, url);
            db.AddInParameter(cmd, "@pid", System.Data.DbType.Int32, pid);
            db.AddInParameter(cmd, "@power", System.Data.DbType.String, power);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@isMenu", System.Data.DbType.Int32, isMenu);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 修改

        public int Update(int id, string name, string url, int pid, string power, int sort, int isMenu, int isEnable, string remark)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Forms set name=@name,url=@url,pid=@pid,power=@power,sort=@sort,isMenu=@isMenu,isEnable=@isEnable,remark=@remark where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@url", System.Data.DbType.String, url);
            db.AddInParameter(cmd, "@pid", System.Data.DbType.Int32, pid);
            db.AddInParameter(cmd, "@power", System.Data.DbType.String, power);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@isMenu", System.Data.DbType.Int32, isMenu);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 查询

        /// <summary>
        /// 获取菜单明细
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.Z_Forms GetModel(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Forms where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Z_Forms model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Z_Forms>(reader);
            }
            return model;
        }

        /// <summary>
        /// 获取指定菜单的直接子类数量
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int GetChildCount(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select count(0) from Z_Forms where pid=@id and isEnable=1";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd));
            }
            catch
            {
                return 1;
            }
        }

        public List<Model.Z_Forms> GetListByWhere(string where)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Forms where " + where + " order by sort asc,id asc";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Z_Forms> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Z_Forms>(reader);
            }
            return list;
        }

        #endregion

        #region 删除

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Delete(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "delete from Z_Forms where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }

    public class Z_Depart
    {
        public int Add(string name, int pid, int sort, string remark)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "insert into Z_Depart (name,pid,sort,remark) values (@name,@pid,@sort,@remark)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@pid", System.Data.DbType.Int32, pid);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@isDel", System.Data.DbType.Int32, 0);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string name, int pid, int sort, string remark)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Depart set name=@name,pid=@pid,sort=@sort,remark=@remark where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@pid", System.Data.DbType.Int32, pid);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        #region 查询

        /// <summary>
        /// 获取部门信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.Z_Depart GetModel(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Depart where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Z_Depart model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Z_Depart>(reader);
            }
            return model;
        }


        /// <summary>
        /// 获取门店（包含父门店名称）
        /// </summary>
        /// <returns></returns>
        public List<Model.Z_Depart> GetListModel()
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Depart order by pid asc,sort asc,id asc";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Z_Depart> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Z_Depart>(reader);
            }
            return list;
        }

        public List<Model.Z_Depart> GetListByWhere(string where)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Depart where " + where + " order by sort asc,id asc";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Z_Depart> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Z_Depart>(reader);
            }
            return list;
        }

        public int GetChildCount(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select count(0) from Z_Depart where pid=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 1);
            }
            catch
            {
                return 1;
            }
        }

        #endregion

        #region 删除

        public int Delete(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "delete Z_Depart where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }

    public class Z_Role
    {
        public int Add(string name, int isEnable, int sort, string remark)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "insert into Z_Role (name,power,isEnable,sort,remark) values (@name,@power,@isEnable,@sort,@remark)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@power", System.Data.DbType.String, "");
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string name, int isEnable, int sort, string remark)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Role set name=@name,isEnable=@isEnable,sort=@sort,remark=@remark where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@sort", System.Data.DbType.Int32, sort);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int SetPower(int id, string power)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Role set power=@power where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@power", System.Data.DbType.String, power);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Z_Role GetModel(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Role where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Z_Role model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Z_Role>(reader);
            }
            return model;
        }

        //public List<Model.Z_Role> GetList(int? isEnable)
        //{
        //    Database db = DatabaseFactory.CreateDatabase();
        //    string sql = isEnable == null ? "select * from Z_Role order by sort asc,id asc" : "select * from Z_Role where isEnable=@isEnable order by sort asc,id asc";
        //    DbCommand cmd = db.GetSqlStringCommand(sql);
        //    if (isEnable != null) db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);

        //    List<Model.Z_Role> list = new List<Model.Z_Role>();
        //    using (var reader = db.ExecuteReader(cmd))
        //    {
        //        list = Tool.DataReaderToModel.ReaderToListModel<Model.Z_Role>(reader);
        //    }
        //    return list;
        //}

        public List<Model.Z_Role> GetListByWhere(string where)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Role where " + where + " order by sort asc,id asc";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Z_Role> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Z_Role>(reader);
            }
            return list;
        }

        public int Delete(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "delete from Z_Role where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }
    }

    public class Z_Users
    {
        public int Add(string username, string userpswd, string realname, string mobile, string email, int depart, int role, int isEnable, int isDptMgr, string remark, int jbid)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "insert into Z_Users (username,userpswd,realname,mobile,email,count,depart,role,isEnable,isDptMgr,addTime,remark,jbid,isDel) values (@username,@userpswd,@realname,@mobile,@email,@count,@depart,@role,@isEnable,@isDptMgr,@addTime,@remark,@jbid,@isDel)";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, Falcon.Function.Encrypt(userpswd));
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@count", System.Data.DbType.Int32, 0);
            db.AddInParameter(cmd, "@depart", System.Data.DbType.Int32, depart);
            db.AddInParameter(cmd, "@role", System.Data.DbType.Int32, role);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@isDptMgr", System.Data.DbType.Int32, isDptMgr);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, DateTime.Now);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@isDel", System.Data.DbType.Int32, 0);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string realname, string mobile, string email, int depart, int role, int isEnable, int isDptMgr, string remark, int jbid)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Users set realname=@realname,mobile=@mobile,email=@email,depart=@depart,role=@role,isEnable=@isEnable,isDptMgr=@isDptMgr,remark=@remark,jbid=@jbid where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@count", System.Data.DbType.Int32, 0);
            db.AddInParameter(cmd, "@depart", System.Data.DbType.Int32, depart);
            db.AddInParameter(cmd, "@role", System.Data.DbType.Int32, role);
            db.AddInParameter(cmd, "@isEnable", System.Data.DbType.Int32, isEnable);
            db.AddInParameter(cmd, "@isDptMgr", System.Data.DbType.Int32, isDptMgr);
            db.AddInParameter(cmd, "@addTime", System.Data.DbType.String, DateTime.Now);
            db.AddInParameter(cmd, "@remark", System.Data.DbType.String, remark);
            db.AddInParameter(cmd, "@jbid", System.Data.DbType.Int32, jbid);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Update(int id, string realname, string mobile, string email)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Users set realname=@realname,mobile=@mobile,email=@email where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@mobile", System.Data.DbType.String, mobile);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int UpdatePswd(int id, string userpswd)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Users set userpswd=@userpswd where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, Falcon.Function.Encrypt(userpswd));
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Z_Users GetModel(int id)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from V_Users_Depart_Role where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.Z_Users model = new Model.Z_Users();
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Z_Users>(reader);
            }
            return model;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="where">isDel=0</param>
        /// <returns></returns>
        public List<Model.Z_Users> GetListByWhere(string where)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from V_Users_Depart_Role where isDel=0 " + (string.IsNullOrEmpty(where)?"":(" and "+where));
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.Z_Users> list;
            using (var reader = db.ExecuteReader(cmd))
            {
                list = Tool.DataReaderToModel.ReaderToListModel<Model.Z_Users>(reader);
            }
            return list;
        }

        public int GetCountByWhere(string where)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select count(0) from Z_Users where isDel=0 and " + where;
            DbCommand cmd = db.GetSqlStringCommand(sql);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd));
            }
            catch
            {
                return 0;
            }
        }

        public Model.Z_Users Login(string username)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Users where username=@username and isDel=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);

            Model.Z_Users model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Z_Users>(reader);
            }
            return model;
        }

        public Model.Z_Users Login(string username, string userpswd)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select * from Z_Users where username=@username and userpswd=@userpswd and isDel=0";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            db.AddInParameter(cmd, "@userpswd", System.Data.DbType.String, Falcon.Function.Encrypt(userpswd));

            Model.Z_Users model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Z_Users>(reader);
            }
            return model;
        }

        public int isExist_UserName(int id, string username)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select count(0) from Z_Users where isDel=0 and id!=@id and username=@username";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);

            try
            {
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 1);
            }
            catch
            {
                return 1;
            }
        }

        public int Delete(int id, DbTransaction tran)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Users set isDel=1 where role!=0 and id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }
    }

    public class Z_Settings
    {
        public int Update(string name, string logo, string image, string smtp, string email, string pswd)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "update Z_Settings set name=@name,logo=@logo,image=@image,smtp=@smtp,email=@email,pswd=@pswd";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@logo", System.Data.DbType.String, logo);
            db.AddInParameter(cmd, "@image", System.Data.DbType.String, image);
            db.AddInParameter(cmd, "@smtp", System.Data.DbType.String, smtp);
            db.AddInParameter(cmd, "@email", System.Data.DbType.String, email);
            db.AddInParameter(cmd, "@pswd", System.Data.DbType.String, pswd);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public Model.Z_Settings GetModel_Top()
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "select top 1 * from Z_Settings";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            Model.Z_Settings model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.Z_Settings>(reader);
            }
            return model;
        }
    }

    public class Z_LoginLogs
    {
        public int Add(int uid, string username, string realname, string ip)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "insert into Z_LoginLogs (uid,username,realname,ip,addTime) values (@uid,@username,@realname,@ip,getdate())";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@uid", System.Data.DbType.Int32, uid);
            db.AddInParameter(cmd, "@username", System.Data.DbType.String, username);
            db.AddInParameter(cmd, "@realname", System.Data.DbType.String, realname);
            db.AddInParameter(cmd, "@ip", System.Data.DbType.String, ip);

            try
            {
                return db.ExecuteNonQuery(cmd);
            }
            catch
            {
                return 0;
            }
        }

        public int Delete(int id, DbTransaction tran)
        {
            Database db = DatabaseFactory.CreateDatabase();
            string sql = "delete Z_LoginLogs where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }
    }
}
