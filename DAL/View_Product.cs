﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common; 
using System.Data;

namespace DAL{ 
	public class View_Product
	{
		Database db;
		
		public View_Product()
		{
		 	db = DatabaseFactory.CreateDatabase();
		}

		 
		#region 查询
        public DataTable getDTbyCon(string where)
        {
            string sql = "select * from View_Product ";
            if (where.Length != 0) sql += (" where " + where);
            DbCommand cmd = db.GetSqlStringCommand(sql);

            return db.ExecuteDataSet(cmd).Tables[0];
        }
		public List<Model.View_Product> GetListModel(string where)
		{
		 	string sql="select * from View_Product ";
			if(where.Length!=0) sql+=(" where "+where);
			DbCommand cmd = db.GetSqlStringCommand(sql);
			   
			List<Model.View_Product> listmodel;
			using (var reader = db.ExecuteReader(cmd))
            {
                listmodel = Tool.DataReaderToModel.ReaderToListModel<Model.View_Product>(reader);
            }
            return listmodel;
		}
		#endregion		
		
	}
}