﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

namespace DAL
{
    public class U_JZX
    {
        Database db;

        public U_JZX()
        {
            db = DatabaseFactory.CreateDatabase();
        }

        #region 增加

        public int Add(string name, int rongliang, int chuliang, string riqi, int addren, DateTime addtime, int daynum, string xiangbian, DateTime chudate, int pdaid, int isFin, int xid, DbTransaction tran)
        {
            string sql = "insert into U_JZX(name,rongliang,chuliang,riqi,addren,addtime,daynum,xiangbian,chudate,pdaid,isFin,xid) values (@name,@rongliang,@chuliang,@riqi,@addren,@addtime,@daynum,@xiangbian,@chudate,@pdaid,@isFin,@xid);select @@identity";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@rongliang", System.Data.DbType.Int32, rongliang);
            db.AddInParameter(cmd, "@chuliang", System.Data.DbType.Int32, chuliang);
            db.AddInParameter(cmd, "@riqi", System.Data.DbType.String, riqi);
            db.AddInParameter(cmd, "@addren", System.Data.DbType.Int32, addren);
            db.AddInParameter(cmd, "@addtime", System.Data.DbType.DateTime, addtime);
            db.AddInParameter(cmd, "@daynum", System.Data.DbType.Int32, daynum);
            db.AddInParameter(cmd, "@xiangbian", System.Data.DbType.String, xiangbian);
            db.AddInParameter(cmd, "@chudate", System.Data.DbType.DateTime, chudate);
            db.AddInParameter(cmd, "@pdaid", System.Data.DbType.Int32, pdaid);
            db.AddInParameter(cmd, "@isFin", System.Data.DbType.Int32, isFin);
            db.AddInParameter(cmd, "@xid", System.Data.DbType.Int32, xid);

            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteScalar(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteScalar(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 修改

        public int Update(int id, string name, int rongliang, int chuliang, string riqi, int addren, DateTime addtime, int daynum, string xiangbian, DateTime chudate, int pdaid, int isFin, int xid, DbTransaction tran)
        {
            string sql = "update U_JZX set name=@name,rongliang=@rongliang,chuliang=@chuliang,riqi=@riqi,addren=@addren,addtime=@addtime,daynum=@daynum,xiangbian=@xiangbian,chudate=@chudate,pdaid=@pdaid,isFin=@isFin,xid=@xid where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);
            db.AddInParameter(cmd, "@name", System.Data.DbType.String, name);
            db.AddInParameter(cmd, "@rongliang", System.Data.DbType.Int32, rongliang);
            db.AddInParameter(cmd, "@chuliang", System.Data.DbType.Int32, chuliang);
            db.AddInParameter(cmd, "@riqi", System.Data.DbType.String, riqi);
            db.AddInParameter(cmd, "@addren", System.Data.DbType.Int32, addren);
            db.AddInParameter(cmd, "@addtime", System.Data.DbType.DateTime, addtime);
            db.AddInParameter(cmd, "@daynum", System.Data.DbType.Int32, daynum);
            db.AddInParameter(cmd, "@xiangbian", System.Data.DbType.String, xiangbian);
            db.AddInParameter(cmd, "@chudate", System.Data.DbType.DateTime, chudate);
            db.AddInParameter(cmd, "@pdaid", System.Data.DbType.Int32, pdaid);
            db.AddInParameter(cmd, "@isFin", System.Data.DbType.Int32, isFin);
            db.AddInParameter(cmd, "@xid", System.Data.DbType.Int32, xid);


            try
            {
                if (tran == null)
                    return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd), 0);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteNonQuery(cmd, tran), 0);
            }
            catch
            {
                return 0;
            }
        }
        #endregion


        #region 查询
        public int GetMaxIDAtToday(string riqi, DbTransaction tran)
        {
            string sql = "select MAX(daynum) from U_JZX where riqi='" + riqi + "' ";
            DbCommand cmd = db.GetSqlStringCommand(sql);

            if (tran == null)
            {
                return Falcon.Function.ToInt(db.ExecuteDataSet(cmd).Tables[0].Rows[0][0].ToString(), 0);
            }
            else
            {
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return Falcon.Function.ToInt(db.ExecuteDataSet(cmd, tran).Tables[0].Rows[0][0].ToString(), 0);
            }
        }
        public Model.U_JZX GetModel(int id)
        {
            string sql = "select * from U_JZX where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            Model.U_JZX model;
            using (var reader = db.ExecuteReader(cmd))
            {
                model = Tool.DataReaderToModel.ReaderToModel<Model.U_JZX>(reader);
            }
            return model;
        }
        public Model.U_JZX GetModelByCon(string where, DbTransaction tran)
        {
            if (string.IsNullOrEmpty(where)) { return null; }
            else
            {
                string sql = "select * from U_JZX ";
                if (where.Length != 0) sql += (" where " + where);
                DbCommand cmd = db.GetSqlStringCommand(sql);
                Model.U_JZX model;
                if (tran == null)
                {
                    using (var reader = db.ExecuteReader(cmd))
                    {
                        model = Tool.DataReaderToModel.ReaderToModel<Model.U_JZX>(reader);
                    }
                }
                else
                {
                    cmd.Transaction = tran;
                    cmd.Connection = tran.Connection;
                    using (var reader = db.ExecuteReader(cmd, tran))
                    {
                        model = Tool.DataReaderToModel.ReaderToModel<Model.U_JZX>(reader);
                    }
                }
                return model;
            }
        }
        public List<Model.U_JZX> GetListModel(string where)
        {
            string sql = "select * from U_JZX ";
            if (where.Length != 0) sql += (" where " + where);
            DbCommand cmd = db.GetSqlStringCommand(sql);

            List<Model.U_JZX> listmodel;
            using (var reader = db.ExecuteReader(cmd))
            {
                listmodel = Tool.DataReaderToModel.ReaderToListModel<Model.U_JZX>(reader);
            }
            return listmodel;
        }
        #endregion

        #region  删除

        public int Delete(int id, DbTransaction tran)
        {
            string sql = "delete from U_JZX where id=@id";
            DbCommand cmd = db.GetSqlStringCommand(sql);
            db.AddInParameter(cmd, "@id", System.Data.DbType.Int32, id);

            try
            {
                if (tran == null)
                    return db.ExecuteNonQuery(cmd);
                cmd.Transaction = tran;
                cmd.Connection = tran.Connection;
                return db.ExecuteNonQuery(cmd, tran);
            }
            catch
            {
                return 0;
            }
        }
        #endregion
    }
}